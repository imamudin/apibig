<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	public function __construct() {
		parent::__construct();
	}

	private function checkauth() {
		if (!$this->session->userdata('is_login')) {
			redirect('/login', 'refresh');
		}
	}
	public function login() {
		return "login";
	}

	public function makeOutput($response){
        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($response))
                ->_display();
        exit;
    }

    public function jsonNoRespon(){
    	$respon = array(
    		"status"	=> 0,
    		"message"	=> "Application not avaliable."
    	);
        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($respon))
                ->_display();
        exit;
    }

	public function checkApp($name){
		if($name==appName){
			return true;
		}else{
			return false;
		}
	}
	public function sendNotification($registration_ids, $message) {        //registatoin_ids type array, message type object
        // Set POST request variable
        $url = 'https://fcm.googleapis.com/fcm/send';

        $headers = array(
            'Authorization: key=' . GOOGLE_API_KEY,
            'Content-Type: application/json'
        );

        $fields = array(
            'registration_ids' => $registration_ids,
            'data' => $message
        );

        $this->openCurl($url, $header, $fields);
        
    }
    public function sendNotificationKolektor($registration_ids, $message) {        //registatoin_ids type array, message type object
        // Set POST request variable
        $url = 'https://fcm.googleapis.com/fcm/send';

        $headers = array(
            'Authorization: key=' . GOOGLE_API_KEY_BIG_KOLEKTOR,
            'Content-Type: application/json'
        );

        $fields = array(
            'registration_ids' => $registration_ids,
            'data' => $message
        );

        $this->openCurl($url, $headers, $fields);
        
    }
    public function sendNotificationSMSPelunasan($nomorHP, $message) {        //registatoin_ids type array, message type object
        // Set POST request variable
        // semua variabel ada di constants.php
        $url = URL_SMS_NOTIFIKASI;

        $headers = array(
            'Authorization: Basic ' . AKUN_SMS_NOTIFIKASI,
            'Content-Type: application/json',
            'Accept : application/json'
        );

        $fields = array(
            'from' => FROM_SMS_NOTIFIKASI,
            'to' => $nomorHP,
            'text' => $message
        );

        $this->openCurl($url, $headers, $fields);
        
    }

    public function openCurl($url, $headers, $fields){
        // Open connection
        $ch = curl_init();
 
        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
 
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 
        // disable SSL certificate support
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
 
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
 
        // execute post

        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
 
        // Close connection
        curl_close($ch);
        return $result;
    }
    public function checkInsPenjualan($name){
		if($name==appJualName){
			return true;
		}else{
			return false;
		}
	}
}