<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Permission extends MY_Controller {
    //method checkApp terletak di MY_Controller

    function __construct(){
        parent::__construct();
        $this->load->model('Permissionmodel');
    } 
    public function login()
    {
        //untuk mengecek apakah input dari aplikasi android
        if($this->checkApp($this->input->get_request_header('appToken'))){
            $status     = -1;
            $message    = '';
            $data       = '';

            $data = (object)json_decode(file_get_contents('php://input'));
            
            isset($data->user_name)     ? $username     = $data->user_name      : $username     = '';
            isset($data->user_password) ? $userpassword = $data->user_password  : $userpassword = '';

            $userlogin = $this->Permissionmodel->login($username, $userpassword);

            if($userlogin){
                $date = date('YmdHis');
                $token = md5($date);

                //megnupdate token user
                if($this->Permissionmodel->updateToken($username, $userpassword, $token)){
                    //mencetak data user
                    $data_user  = $this->Permissionmodel->getuser($username, $userpassword);

                    $id_user    = $data_user[0]->id_user;
                    $tahun_bulan= date("Ym");
                    $bulan      = date("m");

                    $data_target= $this->Permissionmodel->getTarget($id_user,$tahun_bulan, $bulan);
                    
                    $bulan_string   = unserialize (bulan_string);
                    $bulan_string   = $bulan_string[''.$bulan];

                    $waktu      = array(
                            "tahun" => date("Y"),
                            "bulan" => $bulan,
                            "bulan_string"  => $bulan_string
                        );

                    $data_target = array(
                            "waktu"     => $waktu,
                            "data"      => $data_target
                        );

                    $status     = 1;
                    $message    = "Login success.";
                    $data       = array(
                            'user'      => $data_user[0],
                            'target'    => $data_target,
                        );
                }else{
                    $status     = 0;
                    $message    = "Tidak dapat melakukan input data.";
                    $data       = null;
                }
            }else{
                $message    = 'Username dan password tidak ditemukan.';
                $data       = null;
                $status     = 0;
            }
            $response   = array(
                    'status'    => $status,
                    'message'   => $message,
                    'data'      => $data
                );
            $this->makeOutput($response);
        }else{
            $this->jsonNoRespon();
        }
    }
    public function logout(){
        //untuk mengecek apakah input dari aplikasi android
        if($this->checkApp($this->input->get_request_header('appToken'))){
            $status     = -1;
            $message    = '';
            $data       = '';

            $data = (object)json_decode(file_get_contents('php://input'));
            
            isset($data->id_user)       ? $id_user      = $data->id_user        : $id_user      = '';
            isset($data->user_token)    ? $user_token   = $data->user_token     : $user_token   = '';

            $userlogout     = $this->Permissionmodel->logout($id_user, $user_token);

            if($userlogout){
                $message    = 'Logout berhasil.';
                $data       = 'null';
                $status     = 1;
            }else{
                $message    = 'Logout gagal.';
                $data       = null;
                $status     = -1;
            }

            $response   = array(
                    'status'    => $status,
                    'message'     => $message,
                    'data'      => $data
                );
            $this->makeOutput($response);
        }else{
            $this->jsonNoRespon();
        }
    }
}