<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Permissionmodel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function login($username, $password){
        $stat   = "select id_user, id_role, user_name, avatar, user_token from tb_user where binary user_name = '$username' and binary password = '$password' and id_role=3";     //role 3 untuk sales
        $data = $this->db->query($stat);

        if($data->num_rows()==1){
            return $data->result();
        }else{
            return false;
        }
    }
    public function updateToken($username, $password, $token){
        $query = "update tb_user set user_token='$token' 
                    where user_name='$username' and password = '$password'";
        if($this->db->query($query)){
            return true;
        }else{
            return false;
        }
    }
    public function getUser($username, $password){
        $stat   = "select id_user, id_role, user_name, avatar, user_token from tb_user where user_name = '$username' and password = '$password' and id_role=3";
        $data = $this->db->query($stat);

        if($data->num_rows()==1){
            return $data->result();
        }else{
            return false;
        }
    }

    public  function logout($id_user, $user_token){
        $sql    = "select * from tb_user where id_user='".$id_user."' or user_token='".$user_token."'";
        $data   = $this->db->query($sql);
        if($data->num_rows()==1){
            $data = $data->result();
            $data = $data[0];

            if($this->updateToken($data->USER_NAME, $data->PASSWORD, '')){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    public function getTarget($id_user,$tahun_bulan, $bulan){           //ada dua tempat, di Apimodel juga ada
        $sql = "select y.nama NAMA_PEGAWAI, 
                ifnull(sum(TOTAL_TARGET),0) TOTAL_TARGET,  ifnull(sum(TOTAL_REALISASI),0) TOTAL_REALISASI, 
                ifnull(sum(JUMLAH_ORDER),0) JUMLAH_ORDER, ifnull(sum(TOTAL_TRANSAKSI),0) TOTAL_TRANSAKSI
                from (
                SELECT  a.NIK,  0 TOTAL_TARGET, 0 TOTAL_REALISASI, count(a.NIK) JUMLAH_ORDER, ifnull(sum(a.TOTAL),0) TOTAL_TRANSAKSI
                        FROM tb_order a
                where  date_format(tgl_faktur,'%Y%m') = '$tahun_bulan'  and a.NIK='$id_user'
                union all
                SELECT  a.NIK,  0 TOTAL_TARGET,ifnull(sum(b.jumlah),0) TOTAL_REALISASI, 0 JUMLAH_ORDER, 0 TOTAL_TRANSAKSI
                        FROM tb_order a, tb_order_detail b
                where  a.id_order = b.ID_ORDER and date_format(tgl_faktur,'%Y%m') = '$tahun_bulan'  and a.NIK='$id_user'
                union all
                SELECT  a.NIK,  ifnull(sum(a.jumlah),0) TOTAL_TARGET, 0 TOTAL_REALISASI, 0 JUMLAH_ORDER, 0 TOTAL_TRANSAKSI
                        FROM tb_target_order a
                where   bulan = '$tahun_bulan'  and a.NIK='$id_user'
                group by a.NIK ) x, tb_pegawai y
                where x.NIK = y.NIK and y.JOBDESK = 'Sales'";

        $data = $this->db->query($sql);
        if($data->num_rows()>=0){
            $data = $data->result();
            return $data[0];
        }else{
            return false;
        }
    }
}