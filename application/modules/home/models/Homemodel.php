<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Homemodel extends MY_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getDataBerkas($user_id, $kasus_id, $id_doc, $berkas_ke){
    	$sql = "SELECT b.NAMA_PENYIDIK, a.ID_DOC_OPSNAL, c.NAMA_KASUS, a.NRP, a.ID_KASUS, a.BERKAS_KE, a.tgl_masuk_data 
				FROM tb_ba_interogasi a, tb_penyidik b, tb_kasus c 
				WHERE a.NRP=b.NRP
				and a.ID_KASUS=c.ID_KASUS
				and a.ID_KASUS='$kasus_id'
				and a.NRP='$user_id'
				and a.ID_DOC_OPSNAL='$id_doc'
				and a.BERKAS_KE=$berkas_ke";

		$data = $this->db->query($sql)->result();
		if(count($data)==1){
			return $data[0];
		}else{
			return null;
		}
    }

    public function getAllUserReceiveNotif($nrp){
    	return $this->getAllUserReceiveNotofication($nrp);
    }
 //    public function getAllUserReceiveNotofication($nrp) {
 //        $sql   = "SELECT c.*, d.tipe_user
 //                  FROM tb_penyidik c,
 //                       (SELECT NAMA_SATWIL,
 //                               NAMA_SATKER,
 //                               NAMA_UNIT,
 //                               NAMA_SUBDIT
 //                          FROM tb_penyidik a
 //                         WHERE NRP = '$nrp') AS b, tb_user d
 //                 WHERE     c.NAMA_SATWIL = b.NAMA_SATWIL
 //                       AND c.NAMA_SATKER = b.NAMA_SATKER
 //                       AND c.NAMA_UNIT = b.NAMA_UNIT
 //                       AND c.NAMA_SUBDIT = b.NAMA_SUBDIT
 //                       and c.nrp = d.id_user
 //                       union 
 //                 SELECT c.*, d.tipe_user
 //                  FROM tb_penyidik c,
 //                       (SELECT NAMA_SATWIL,
 //                               NAMA_SATKER,
 //                               '' NAMA_UNIT,
 //                               NAMA_SUBDIT
 //                          FROM tb_penyidik a
 //                         WHERE NRP = '$nrp') AS b, tb_user d
 //                 WHERE     c.NAMA_SATWIL = b.NAMA_SATWIL
 //                       AND c.NAMA_SATKER = b.NAMA_SATKER
 //                       AND c.NAMA_SUBDIT = b.NAMA_SUBDIT
 //                       and  c.nrp = d.id_user and d.TIPE_USER = 3
 //                       union
 //                 SELECT c.*, d.tipe_user
 //                  FROM tb_penyidik c,
 //                       (SELECT NAMA_SATWIL,
 //                               NAMA_SATKER,
 //                               '' NAMA_UNIT,
 //                               NAMA_SUBDIT
 //                          FROM tb_penyidik a
 //                         WHERE NRP = '$nrp') AS b, tb_user d
 //                 WHERE     c.NAMA_SATWIL = b.NAMA_SATWIL
 //                       AND c.NAMA_SATKER = b.NAMA_SATKER
 //                       and  c.nrp = d.id_user and d.TIPE_USER = 2";
 //        //and d.user_regId !=''
 //        $data = $this->db->query($sql)->result();
 //        return $data;		
	// }
}