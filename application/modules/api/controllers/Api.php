<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Api extends MY_Controller {

    //method checkApp terletak di MY_Controller

    function __construct() {
        parent::__construct();
        $this->load->model('Apimodel');
    }

    public function teskoneksi() {
        echo "koneksi berhasil!";
    }

    public function scantoko() {
        //untuk mengecek apakah input dari aplikasi android
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->KODE) ? $KODE = $data->KODE : $KODE = '';
            isset($data->LATITUDE) ? $LATITUDE = $data->LATITUDE : $LATITUDE = '';
            isset($data->LONGITUDE) ? $LONGITUDE = $data->LONGITUDE : $LONGITUDE = '';


            if ($id_user != '' && $user_token != '' && $KODE != '' && $LATITUDE != '' && $LONGITUDE != '') {
                //mengecek waktu scan, harus diantara jam 08.00 - 15.30
                $jam    = date('H');
                if($jam >= 8 && $jam <= 17){
                    $menit  = date('i');
                    if($jam == 15 && ($menit >= 30)){
                        //tidak bisa scan karena lebih dari jam setengah 4
                        $message = 'Maaf, transaksi pelanggan melebihi jam yang ditentukan (08:00 s/d 17:30)';
                        $data = null;
                        $status = 0;
                    }else{
                        //mengecek login user
                        $is_login = $this->Apimodel->getUserByToken($id_user, $user_token);
                        if ($is_login != false) {
                            //mengecek apakah ID Sales sesuai dengan toko atau tidak
                            $toko = $this->Apimodel->getTokobyKode($KODE);
                            $long = 0;
                            $lati = 0;

                            if (count($toko) == 1) {
                                if($toko[0]->KDSALES == $id_user){
                                    //mengecek apakah longitude dan latitude null
                                    if(is_null($toko[0]->LONGITUDE)|| is_null($toko[0]->LATITUDE)){
                                        //merubah koordinat toko, karena nilai awal null
                                        
                                        $LATITUDE   = (double)$LATITUDE;
                                        $LONGITUDE  = (double)$LONGITUDE;
                                        
                                        if($this->Apimodel->updateKoordinat($KODE, $LONGITUDE, $LATITUDE)){
                                            //baris ini sama pada longitude dan latitude tidak null
                                            $datetime = date('Y-m-d H:i:s');
                                            $tanggal = substr($datetime, 0, 10);
                                            $jam_mulai = substr($datetime, 11);

                                            $dataKunjungan = $this->Apimodel->insertKunjungan($id_user, $KODE, $LATITUDE, $LONGITUDE, $tanggal, $jam_mulai, 1);

                                            if ($dataKunjungan) {
                                                $poinToko = $this->Apimodel->getPoin($KODE);
                                                $piutangToko = $this->Apimodel->getPiutang($KODE);
                                                if ($poinToko) {
                                                    $newdata = array(
                                                        'kunjungan' => $dataKunjungan,
                                                        'poin' => $poinToko,
                                                        'piutang' => $piutangToko,
                                                    );
                                                    $message = "Scan berhasil.";
                                                    $data = $newdata;
                                                    $status = 1;
                                                } else {
                                                    $message = "Data toko tidak ditemukan.";
                                                    $data = null;
                                                    $status = 0;
                                                }
                                            } else {
                                                $message = "Tidak dapat melakukan input data.";
                                                $data = null;
                                                $status = 0;
                                            }
                                        }else{
                                            $message = "Tidak dapat merubah koordinat.";
                                            $data = null;
                                            $status = 0;
                                        }
                                    }else{
                                        $long = (double)$toko[0]->LONGITUDE;
                                        $lati = (double)$toko[0]->LATITUDE;

                                        $LATITUDE   = (double)$LATITUDE;
                                        $LONGITUDE  = (double)$LONGITUDE;

                                        $jarak  = $this->hitungKoordinat($LATITUDE, $LONGITUDE, $lati, $long);
                                        
                                        if ($jarak < maksimalJarak) {
                                            //jika kurang dari 50 meter dapat melakukan kunjungan
                                            //baris ini sama pada longitude dan latitude null
                                            $datetime = date('Y-m-d H:i:s');
                                            $tanggal = substr($datetime, 0, 10);
                                            $jam_mulai = substr($datetime, 11);

                                            $dataKunjungan = $this->Apimodel->insertKunjungan($id_user, $KODE, $LATITUDE, $LONGITUDE, $tanggal, $jam_mulai, 1);

                                            if ($dataKunjungan) {
                                                $poinToko = $this->Apimodel->getPoin($KODE);
                                                $piutangToko = $this->Apimodel->getPiutang($KODE);
                                                if ($poinToko) {
                                                    $newdata = array(
                                                        'kunjungan' => $dataKunjungan,
                                                        'poin' => $poinToko,
                                                        'piutang' => $piutangToko,
                                                    );
                                                    $message = "Scan berhasil.";
                                                    $data = $newdata;
                                                    $status = 1;
                                                } else {
                                                    $message = "Data toko tidak ditemukan.";
                                                    $data = null;
                                                    $status = 0;
                                                }
                                            } else {
                                                $message = "Tidak dapat melakukan input data.";
                                                $data = null;
                                                $status = 0;
                                            }
                                        } else {
                                            //jika lebih dari 50 meter tidak dapat melakukan kunjungan
                                            $message = "Barcode yang discan tidak berada di lokasi pelanggan.";//" jarak : " . $jarak . ', maks : ';// . maksimalJarak;
                                            $data = null;
                                            $status = 0;
                                        }
                                    }
                                }else{
                                    $message = 'Maaf, barcode yang anda scan bukan pelanggan anda.';
                                    $data = null;
                                    $status = 0;
                                }
                            }else{
                                $message = 'Data Toko tidak ditemukan.';
                                $data = null;
                                $status = 0;
                            }
                        } else {
                            $message = 'Akun tidak ditemukan.';
                            $data = null;
                            $status = 0;
                        }
                    }
                }else{
                    $message = 'Maaf, transaksi pelanggan melebihi jam yang ditentukan (08:00 s/d 17:30)';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }

    public static function vincentyGreatCircleDistance(
    $latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000) {
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $lonDelta = $lonTo - $lonFrom;
        $a = pow(cos($latTo) * sin($lonDelta), 2) +
                pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
        $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

        $angle = atan2(sqrt($a), $b);
        return $angle * $earthRadius;
    }

    private function hitungKoordinat($latFrom, $longFrom, $latTo, $longTo) {
        $latitudeFrom = $latFrom;
        $longitudeFrom = $longFrom;

        $latitudeTo = $latTo;
        $longitudeTo = $longTo;

        //Calculate distance from latitude and longitude
        $theta = $longitudeFrom - $longitudeTo;
        $dist = sin(deg2rad($latitudeFrom)) * sin(deg2rad($latitudeTo)) + cos(deg2rad($latitudeFrom)) * cos(deg2rad($latitudeTo)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;

        $distance = ($miles * 1.609344) * 1000; //return to meter

        return $distance;
    }
    function get_distance_between_points($latitude1, $longitude1, $latitude2, $longitude2) {
        echo "$latitude1 <br>";
        echo "$longitude1 <br>";
        echo "$latitude2 <br>";
        echo "$longitude2 <br>";
        echo "<br>";

        $theta = $longitude1 - $longitude2;
        $miles = (sin(deg2rad($latitude1)) * sin(deg2rad($latitude2))) + (cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($theta)));
        $miles = acos($miles);
        $miles = rad2deg($miles);
        $miles = $miles * 60 * 1.1515;
        $kilometers = $miles * 1.609344;
        return $kilometers; 
    }

    public function tutupKunjungan() {
        //untuk mengecek apakah input dari aplikasi android
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->KODE) ? $KODE = $data->KODE : $KODE = '';
            isset($data->TGL_KUNJUNGAN) ? $TGL_KUNJUNGAN = $data->TGL_KUNJUNGAN : $TGL_KUNJUNGAN = '';
            isset($data->JAM_MULAI) ? $JAM_MULAI = $data->JAM_MULAI : $JAM_MULAI = '';


            if ($id_user != '' && $user_token != '' && $KODE != '' && $TGL_KUNJUNGAN != '' && $JAM_MULAI != '') {
                //mengecek login user
                $is_login = $this->Apimodel->getUserByToken($id_user, $user_token);
                if ($is_login) {
                    $jam_tutup = date('H:i:s');
                    $update_kunjungan = $this->Apimodel->updateKunjungan($id_user, $KODE, $TGL_KUNJUNGAN, $JAM_MULAI, $jam_tutup);

                    if ($update_kunjungan) {
                        $message = "Data berhasil disimpan.";
                        $data = null;
                        $status = 1;
                    } else {
                        $message = "Tidak dapat mengubah data.";
                        $data = null;
                        $status = 0;
                    }
                } else {
                    $message = 'Akun tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }

    public function formInputStok() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $produk = $this->Apimodel->getProduk();

            if ($produk) {
                $status = 1;
                $message = 'Data form input stok.';
                $newdata = array(
                    'produk' => $produk,
                );
                $data = $newdata;
            } else {
                $status = 0;
                $message = 'Tidak dapat menemukan data.';
                $data = null;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }

    public function inputStok() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->KODE) ? $KODE = $data->KODE : $KODE = '';
            isset($data->STOK) ? $STOK = $data->STOK : $STOK = array();
            isset($data->ID_PRODUK) ? $ID_PRODUK = $data->ID_PRODUK : $ID_PRODUK = array();

            if ($id_user != '' && $user_token != '' && $KODE != '' && count($STOK) > 0 && count($STOK) > 0) {
                //mengecek login user
                $is_login = $this->Apimodel->getUserByToken($id_user, $user_token);
                if ($is_login) {
                    $tanggal = date('Y-m-d');
                    //menghapus stok yang pernah tersimpan sebelumnya
                    $delete_stok = $this->Apimodel->hapus_stok($KODE, $tanggal);

                    for ($i = 0; $i < count($STOK); $i++) {
                        $this->Apimodel->insertStok($KODE, $ID_PRODUK[$i], $tanggal, 0, '', $STOK[$i]);
                    }
                    $message = "Data berhasil disimpan.";
                    $data = null;
                    $status = 1;
                } else {
                    $message = 'Akun tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }

    public function cekFaktur() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->KODE) ? $KODE = $data->KODE : $KODE = '';

            if ($id_user != '' && $user_token != '' && $KODE != '') {
                //mengecek login user
                $is_login = $this->Apimodel->getUserByToken($id_user, $user_token);
                if ($is_login) {
                    //mengecek faktur dari toko, apabila ada return true, dapat melakukan order, jika tidak, tidak dapat
                    $faktur = $this->Apimodel->cekFaktur($KODE);

                    // if($adaFaktur){
                    // }else{
                    //     $message    = "Pelanggan belum melakukan pembayaran piutang sesuai jatuh tempo.\nOrder pelanggan selanjutnya menunggu appoval dari perusahaan. apakah akan melanjutkan proses order ?";
                    // }

                    $harga = $this->Apimodel->getHarga();
                    $gudang = $this->Apimodel->getGudang();
                    $total_piutang = $this->Apimodel->getTotalPiutang($KODE);
                    $default_gudang = $this->Apimodel->getDefaultGudang($id_user);

                    $status = 1;
                    $message = 'Data form order.';
                    $newdata = array(
                        'harga' => $harga,
                        'gudang' => $gudang,
                        'total_piutang' => $total_piutang,
                        'default_gudang' => $default_gudang,
                        'ada_faktur' => $faktur
                    );

                    $data = $newdata;
                } else {
                    $message = 'Akun tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }
    public function simpanOrder() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->jumlah_pesanan) ? $jumlah_pesanan = $data->jumlah_pesanan : $jumlah_pesanan = array();
            isset($data->HARGA) ? $HARGA = $data->HARGA : $HARGA = array();
            isset($data->ID_PRODUK) ? $ID_PRODUK = $data->ID_PRODUK : $ID_PRODUK = array();
            isset($data->tanggal_kirim) ? $tanggal_kirim = $data->tanggal_kirim : $tanggal_kirim = '';
            isset($data->JENIS_PENGIRIMAN) ? $JENIS_PENGIRIMAN = $data->JENIS_PENGIRIMAN : $JENIS_PENGIRIMAN = '';
            isset($data->KODE) ? $KODE = $data->KODE : $KODE = '';       //untuk kode toko
            isset($data->total_harga) ? $total_harga = $data->total_harga : $total_harga = '';
            isset($data->KODE_GUDANG) ? $KODE_GUDANG = $data->KODE_GUDANG : $KODE_GUDANG = '';
            isset($data->is_approval) ? $is_approval = $data->is_approval : $is_approval = 1;

            if ($id_user != '' && $user_token != '' && $tanggal_kirim != '' && $jumlah_pesanan != '' && $KODE_GUDANG != '' && $KODE != '' && $total_harga != '' && count($HARGA) > 0 && count($jumlah_pesanan) > 0 && count($ID_PRODUK) > 0 && $JENIS_PENGIRIMAN != '') {
                //mengecek login user
                $is_login = $this->Apimodel->getUserByToken($id_user, $user_token);
                if ($is_login) {
                    //pertama insert ke tb order
                    $tanggal = date('Y-m-d');
                    $tanggal_id = date('ymd');
                    $order_ke = ($this->Apimodel->totalOrderHariIni($tanggal_id) + 1);

                    //membuat faktur
                    // $tahun_bulan = date("Ym");
                    // $bulan = date('m');

                    // if ($is_approval == 1) {
                    //     if ($JENIS_PENGIRIMAN == CD) {
                    //         $bulan_romawi = unserialize(bulan_romawi);

                    //         $bulan_romawi = $bulan_romawi['' . $bulan];

                    //         $setengah_faktur = $tahun_bulan . '/' . $bulan_romawi . '/' . $KODE_GUDANG;

                    //         $faktur_ke = ($this->Apimodel->getTotalFakturBulanIni($setengah_faktur) + 1);
                    //         ;

                    //         if ($faktur_ke < 10) {
                    //             $faktur = $setengah_faktur . '000' . $faktur_ke;
                    //         } else if ($faktur_ke >= 10 && $faktur_ke < 100) {
                    //             $faktur = $setengah_faktur . '00' . $faktur_ke;
                    //         } else if ($faktur_ke >= 100 && $faktur_ke < 1000) {
                    //             $faktur = $setengah_faktur . '0' . $faktur_ke;
                    //         } else {
                    //             $faktur = $setengah_faktur . '' . $faktur_ke;
                    //         }
                    //     } else if ($JENIS_PENGIRIMAN == TRONTON) {
                    //         $setengah_faktur = $tahun_bulan . $KODE_GUDANG;

                    //         $faktur_ke = ($this->Apimodel->getTotalFakturBulanIni($setengah_faktur) + 1);
                    //         ;

                    //         if ($faktur_ke < 10) {
                    //             $faktur = $setengah_faktur . '000' . $faktur_ke;
                    //         } else if ($faktur_ke >= 10 && $faktur_ke < 100) {
                    //             $faktur = $setengah_faktur . '00' . $faktur_ke;
                    //         } else if ($faktur_ke >= 100 && $faktur_ke < 1000) {
                    //             $faktur = $setengah_faktur . '0' . $faktur_ke;
                    //         } else {
                    //             $faktur = $setengah_faktur . '' . $faktur_ke;
                    //         }
                    //     }
                    // } else {
                    //     $faktur = "";
                    // }
                    $faktur = "-";
                    //tutup membuat faktur

                    $tgl_faktur = date('Y-m-d');
                    $jatuh_tempo_toko = $this->Apimodel->getJatuhTempo($KODE);
                    $tgl_jth_tempo = date('Y-m-d', strtotime("+$jatuh_tempo_toko days"));

                    if ($order_ke < 10) {
                        $id_order = $tanggal_id . '000' . $order_ke;
                    } else if ($order_ke >= 10 && $order_ke < 100) {
                        $id_order = $tanggal_id . '00' . $order_ke;
                    } else if ($order_ke >= 100 && $order_ke < 1000) {
                        $id_order = $tanggal_id . '0' . $order_ke;
                    } else {
                        $id_order = $tanggal_id . '' . $order_ke;
                    }

                    $insert_order = $this->Apimodel->insertorder($id_order, $id_user, $KODE, $faktur, $tgl_faktur, $total_harga, $KODE_GUDANG, $tgl_jth_tempo, $tanggal_kirim, $JENIS_PENGIRIMAN, $is_approval);

                    if ($insert_order) {
                        //insert detail order
                        $total_pesanan = 0;
                        for ($i = 0; $i < count($ID_PRODUK); $i++) {
                            $this->Apimodel->insertDetailOrder($id_order, $ID_PRODUK[$i], $jumlah_pesanan[$i], $HARGA[$i], $jumlah_pesanan[$i] * $HARGA[$i]);

                            $total_pesanan += $jumlah_pesanan[$i];
                        }

                        //menghitung poin
                        $bagi_poin = $this->Apimodel->getPoinPerPembelian();
                        $total_poin = ($total_pesanan) / $bagi_poin;
                        $s_poin = "" . $total_poin;
                        $poins = explode('.', $s_poin);
                        $poin = 0;
                        if (count($poins) >= 0) {
                            $poin = $poins[0];
                        }
                        //menyimpan stok
                        for ($i = 0; $i < count($ID_PRODUK); $i++) {
                            $insert_stok1 = $this->Apimodel->insertStok($KODE, $ID_PRODUK[$i], $tanggal, 1, $id_order, $jumlah_pesanan[$i]);
                        }
                        //menyimpan poin
                        $insert_poin = $this->Apimodel->insertPoin($KODE, $id_order, ($jumlah_pesanan[0] + $jumlah_pesanan[0]), $poin);

                        $message = "Data berhasil disimpan.";
                        $data = null;
                        $status = 1;
                    } else {
                        $message = "Gagal menyimpan data.";
                        $data = null;
                        $status = 0;
                    }
                } else {
                    $message = 'Akun tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }
    public function cekFakturNonSemen() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->KODE) ? $KODE = $data->KODE : $KODE = '';

            if ($id_user != '' && $user_token != '' && $KODE != '') {
                //mengecek login user
                $is_login = $this->Apimodel->getUserByToken($id_user, $user_token);
                if ($is_login) {
                    //mengecek faktur dari toko, apabila ada return true, dapat melakukan order, jika tidak, tidak dapat
                    $faktur = $this->Apimodel->cekFaktur($KODE);

                    $produk_non_semen   = $this->Apimodel->getProdukNonSemen();
                    $gudang             = $this->Apimodel->getGudang();
                    $total_piutang      = $this->Apimodel->getTotalPiutang($KODE);
                    $default_gudang     = $this->Apimodel->getDefaultGudang($id_user);

                    $status = 1;
                    $message = 'Data form order.';
                    $newdata = array(
                        'produknonsemen'    => $produk_non_semen,
                        'gudang'            => $gudang,
                        'total_piutang'     => $total_piutang,
                        'default_gudang'    => $default_gudang,
                        'ada_faktur'        => $faktur
                    );

                    $data = $newdata;
                } else {
                    $message = 'Akun tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }
    public function simpanOrderNonSemen() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->jumlah_pesanan) ? $jumlah_pesanan = $data->jumlah_pesanan : $jumlah_pesanan = array();
            isset($data->HARGA) ? $HARGA = $data->HARGA : $HARGA = array();
            isset($data->ID_PRODUK) ? $ID_PRODUK = $data->ID_PRODUK : $ID_PRODUK = array();
            isset($data->tanggal_kirim) ? $tanggal_kirim = $data->tanggal_kirim : $tanggal_kirim = '';
            isset($data->KODE) ? $KODE = $data->KODE : $KODE = '';       //untuk kode toko
            isset($data->total_harga) ? $total_harga = $data->total_harga : $total_harga = '';
            isset($data->KODE_GUDANG) ? $KODE_GUDANG = $data->KODE_GUDANG : $KODE_GUDANG = '';
            isset($data->is_approval) ? $is_approval = $data->is_approval : $is_approval = 1;

            if ($id_user != '' && $user_token != '' && $tanggal_kirim != '' 
            && $jumlah_pesanan != '' && $KODE_GUDANG != '' && $KODE != '' 
            && $total_harga != '' && count($HARGA) > 0 && count($jumlah_pesanan) > 0 
            && count($ID_PRODUK) > 0) {
                //mengecek login user
                $is_login = $this->Apimodel->getUserByToken($id_user, $user_token);
                if ($is_login) {
                    //pertama insert ke tb order
                    $tanggal = date('Y-m-d');
                    $tanggal_id = date('ymd');
                    $order_ke = ($this->Apimodel->totalOrderHariIniNonSemen($tanggal_id) + 1);

                    $faktur = "-";
                    //tutup membuat faktur

                    $tgl_faktur = date('Y-m-d');
                    $jatuh_tempo_toko = $this->Apimodel->getJatuhTempo($KODE);
                    $tgl_jth_tempo = date('Y-m-d', strtotime("+$jatuh_tempo_toko days"));

                    if ($order_ke < 10) {
                        $id_order = $tanggal_id . '000' . $order_ke;
                    } else if ($order_ke >= 10 && $order_ke < 100) {
                        $id_order = $tanggal_id . '00' . $order_ke;
                    } else if ($order_ke >= 100 && $order_ke < 1000) {
                        $id_order = $tanggal_id . '0' . $order_ke;
                    } else {
                        $id_order = $tanggal_id . '' . $order_ke;
                    }

                    $insert_order = $this->Apimodel->insertorderNonSemen($id_order, $id_user, $KODE, $faktur, $tgl_faktur, $total_harga, $KODE_GUDANG, $tgl_jth_tempo, $tanggal_kirim, "-", $is_approval);

                    if ($insert_order) {
                        //insert detail order
                        $total_pesanan = 0;
                        for ($i = 0; $i < count($ID_PRODUK); $i++) {
                            $this->Apimodel->insertDetailOrderNonSemen($id_order, $ID_PRODUK[$i], $jumlah_pesanan[$i], $HARGA[$i], $jumlah_pesanan[$i] * $HARGA[$i]);

                            $total_pesanan += $jumlah_pesanan[$i];
                        }

                        $message = "Data berhasil disimpan.";
                        $data = null;
                        $status = 1;
                    } else {
                        $message = "Gagal menyimpan data.";
                        $data = null;
                        $status = 0;
                    }
                } else {
                    $message = 'Akun tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }
    public function getKunjungan() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->KODE) ? $KODE = $data->KODE : $KODE = '';
            isset($data->TGL_AWAL) ? $TGL_AWAL = $data->TGL_AWAL : $TGL_AWAL = '';
            isset($data->TGL_AKHIR) ? $TGL_AKHIR = $data->TGL_AKHIR : $TGL_AKHIR = '';
            isset($data->START) ? $START = $data->START : $START = '';
            isset($data->LIMIT) ? $LIMIT = $data->LIMIT : $LIMIT = '';


            if ($id_user != '' && $user_token != '') {
                //mengecek login user
                $is_login = $this->Apimodel->getUserByToken($id_user, $user_token);
                if ($is_login) {
                    $data_kunjungan = $this->Apimodel->getKunjungans($id_user, $KODE, $TGL_AWAL, $TGL_AKHIR, $START, $LIMIT);
                    // var_dump($data_kunjungan);
                    // exit();
                    if ($data_kunjungan) {
                        $kunjungan = array(
                            "total" => count($data_kunjungan),
                            "kunjungan" => $data_kunjungan
                        );
                        $message = 'Data kunjungan.';
                        $data = $kunjungan;
                        $status = 1;
                    } else {
                        $message = 'Data kunjungan tidak ditemukan.';
                        $data = null;
                        $status = 0;
                    }
                } else {
                    $message = 'Akun tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }

    public function getOrder() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->KODE) ? $KODE = $data->KODE : $KODE = '';
            isset($data->TGL_AWAL) ? $TGL_AWAL = $data->TGL_AWAL : $TGL_AWAL = '';
            isset($data->TGL_AKHIR) ? $TGL_AKHIR = $data->TGL_AKHIR : $TGL_AKHIR = '';
            isset($data->START) ? $START = $data->START : $START = '';
            isset($data->LIMIT) ? $LIMIT = $data->LIMIT : $LIMIT = '';


            if ($id_user != '' && $user_token != '' && $TGL_AWAL != '' && $TGL_AKHIR != '') {
                //mengecek login user
                $is_login = $this->Apimodel->getUserByToken($id_user, $user_token);
                if ($is_login) {
                    $data_kunjungan = $this->Apimodel->getOrders($id_user, $KODE, $TGL_AWAL, $TGL_AKHIR, $START, $LIMIT);
                    // var_dump($data_kunjungan);
                    // exit();
                    if ($data_kunjungan) {
                        $kunjungan = array(
                            "total" => count($data_kunjungan),
                            "order" => $data_kunjungan
                        );
                        $message = 'Data order.';
                        $data = $kunjungan;
                        $status = 1;
                    } else {
                        $message = 'Data order tidak ditemukan.';
                        $data = null;
                        $status = 0;
                    }
                } else {
                    $message = 'Akun tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }

    public function getDetailOrder() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->ID_ORDER) ? $ID_ORDER = $data->ID_ORDER : $ID_ORDER = '';

            if ($id_user != '' && $user_token != '' && $ID_ORDER != '') {
                //mengecek login user
                $is_login = $this->Apimodel->getUserByToken($id_user, $user_token);
                if ($is_login) {
                    $data_order = $this->Apimodel->getDetailOrders($ID_ORDER);
                    if ($data_order) {
                        $order = array(
                            "total" => count($data_order),
                            "order" => $data_order
                        );
                        $message = 'Data detail order.';
                        $data = $order;
                        $status = 1;
                    } else {
                        $message = 'Data detail order tidak ditemukan.';
                        $data = null;
                        $status = 0;
                    }
                } else {
                    $message = 'Akun tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }
    public function getOrderNonSemen() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->KODE) ? $KODE = $data->KODE : $KODE = '';
            isset($data->TGL_AWAL) ? $TGL_AWAL = $data->TGL_AWAL : $TGL_AWAL = '';
            isset($data->TGL_AKHIR) ? $TGL_AKHIR = $data->TGL_AKHIR : $TGL_AKHIR = '';
            isset($data->START) ? $START = $data->START : $START = '';
            isset($data->LIMIT) ? $LIMIT = $data->LIMIT : $LIMIT = '';


            if ($id_user != '' && $user_token != '' && $TGL_AWAL != '' && $TGL_AKHIR != '') {
                //mengecek login user
                $is_login = $this->Apimodel->getUserByToken($id_user, $user_token);
                if ($is_login) {
                    $data_kunjungan = $this->Apimodel->getOrdersNonSemen($id_user, $KODE, $TGL_AWAL, $TGL_AKHIR, $START, $LIMIT);
                    // var_dump($data_kunjungan);
                    // exit();
                    if ($data_kunjungan) {
                        $kunjungan = array(
                            "total" => count($data_kunjungan),
                            "order" => $data_kunjungan
                        );
                        $message = 'Data order.';
                        $data = $kunjungan;
                        $status = 1;
                    } else {
                        $message = 'Data order tidak ditemukan.';
                        $data = null;
                        $status = 0;
                    }
                } else {
                    $message = 'Akun tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }
    public function getDetailOrderNonSemen() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->ID_ORDER) ? $ID_ORDER = $data->ID_ORDER : $ID_ORDER = '';

            if ($id_user != '' && $user_token != '' && $ID_ORDER != '') {
                //mengecek login user
                $is_login = $this->Apimodel->getUserByToken($id_user, $user_token);
                if ($is_login) {
                    $data_order = $this->Apimodel->getDetailOrdersNonSemen($ID_ORDER);
                    if ($data_order) {
                        $order = array(
                            "total" => count($data_order),
                            "order" => $data_order
                        );
                        $message = 'Data detail order.';
                        $data = $order;
                        $status = 1;
                    } else {
                        $message = 'Data detail order tidak ditemukan.';
                        $data = null;
                        $status = 0;
                    }
                } else {
                    $message = 'Akun tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }

    public function getToko() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';
            isset($data->search)        ? $search       = $data->search     : $search   = '';

            isset($data->START) ? $START = $data->START : $START = '';
            isset($data->LIMIT) ? $LIMIT = $data->LIMIT : $LIMIT = '';

            if ($id_user != '' && $user_token != '') {
                $data_toko = $this->Apimodel->getToko($id_user, $START, $LIMIT, $search);
                if ($data_toko) {
                    $order = array(
                        "total" => count($data_toko),
                        "toko" => $data_toko
                    );
                    $message = 'Data toko.';
                    $data = $order;
                    $status = 1;
                } else {
                    $message = 'Data toko tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }

    public function getPiutang() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->KODE) ? $KODE = $data->KODE : $KODE = '';
            isset($data->START) ? $START = $data->START : $START = '';
            isset($data->LIMIT) ? $LIMIT = $data->LIMIT : $LIMIT = '';

            if ($id_user != '' && $user_token != '') {
                //mengecek login user
                $is_login = $this->Apimodel->getUserByToken($id_user, $user_token);
                if ($is_login) {
                    $data_piutang = $this->Apimodel->getPiutangs($id_user, $KODE, $START, $LIMIT);
                    // var_dump($data_kunjungan);
                    // exit();
                    if ($data_piutang) {
                        $kunjungan = array(
                            "total" => count($data_piutang),
                            "piutang" => $data_piutang
                        );
                        $message = 'Data piutang.';
                        $data = $kunjungan;
                        $status = 1;
                    } else {
                        $message = 'Data piutang tidak ditemukan.';
                        $data = null;
                        $status = 0;
                    }
                } else {
                    $message = 'Akun tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }

    public function getDetailPiutang() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->KODE) ? $KODE = $data->KODE : $KODE = '';

            if ($id_user != '' && $user_token != '' && $KODE != '') {
                //mengecek login user
                $is_login = $this->Apimodel->getUserByToken($id_user, $user_token);
                if ($is_login) {
                    $data_piutang = $this->Apimodel->getDetailPiutang($KODE);
                    if ($data_piutang) {
                        $order = array(
                            "total" => count($data_piutang),
                            "piutang" => $data_piutang
                        );
                        $message = 'Data detail Piutang.';
                        $data = $order;
                        $status = 1;
                    } else {
                        $message = 'Data detail piutang tidak ditemukan.';
                        $data = null;
                        $status = 0;
                    }
                } else {
                    $message = 'Akun tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }

    public function getTarget() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            if ($id_user != '' && $user_token != '') {
                //mengecek login user
                $is_login = $this->Apimodel->getUserByToken($id_user, $user_token);
                if ($is_login) {
                    $tahun_bulan = date("Ym");
                    $bulan = date("m");

                    $data_target = $this->Apimodel->getTarget($id_user, $tahun_bulan, $bulan);

                    $bulan_string = unserialize(bulan_string);
                    $bulan_string = $bulan_string['' . $bulan];

                    $waktu = array(
                        "tahun" => date("Y"),
                        "bulan" => $bulan,
                        "bulan_string" => $bulan_string
                    );

                    $data_target = array(
                        "waktu" => $waktu,
                        "data" => $data_target
                    );


                    if ($data_target) {
                        $message = 'Data Target';
                        $data = $data_target;
                        $status = 1;
                    } else {
                        $message = 'Data Target order tidak ditemukan.';
                        $data = null;
                        $status = 0;
                    }
                } else {
                    $message = 'Akun tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'target' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }

    // --------------------------------------dari Mas Pii------------------------------------------
    public function getPoin() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->KODE) ? $KODE = $data->KODE : $KODE = '';
//            isset($data->TGL_AWAL)      ? $TGL_AWAL     = $data->TGL_AWAL       : $TGL_AWAL     = '';
//            isset($data->TGL_AKHIR)     ? $TGL_AKHIR    = $data->TGL_AKHIR      : $TGL_AKHIR    = '';
            isset($data->START) ? $START = $data->START : $START = '';
            isset($data->LIMIT) ? $LIMIT = $data->LIMIT : $LIMIT = '';


            if ($id_user != '' && $user_token != '') {
                //mengecek login user
                $is_login = $this->Apimodel->getUserByToken($id_user, $user_token);
                if ($is_login) {
                    $data_poin = $this->Apimodel->getPoinPelanggan($id_user, $KODE, $START, $LIMIT);
                    // var_dump($data_kunjungan);
                    // exit();
                    if ($data_poin) {
                        $poin = array(
                            "total" => count($data_poin),
                            "poin" => $data_poin
                        );
                        $message = 'Data Poin Pelanggan.';
                        $data = $poin;
                        $status = 1;
                    } else {
                        $message = 'Data poin pelanggan tidak ditemukan.';
                        $data = null;
                        $status = 0;
                    }
                } else {
                    $message = 'Akun tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }

    public function getDetailPoin() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->KODE) ? $KODE = $data->KODE : $KODE = '';

            if ($id_user != '' && $user_token != '' && $KODE != '') {
                //mengecek login user
                $is_login = $this->Apimodel->getUserByToken($id_user, $user_token);
                if ($is_login) {
                    $data_poin = $this->Apimodel->getDetailPoin($KODE);
                    if ($data_poin) {
                        $order = array(
                            "total" => count($data_poin),
                            "poin" => $data_poin
                        );
                        $message = 'Data detail poin pelanggan.';
                        $data = $order;
                        $status = 1;
                    } else {
                        $message = 'Data detail poin tidak ditemukan.';
                        $data = null;
                        $status = 0;
                    }
                } else {
                    $message = 'Akun tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }

    public function getDetailTukar() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->KODE) ? $KODE = $data->KODE : $KODE = '';

            if ($id_user != '' && $user_token != '' && $KODE != '') {
                //mengecek login user
                $is_login = $this->Apimodel->getUserByToken($id_user, $user_token);
                if ($is_login) {
                    $data_tukar = $this->Apimodel->getDetailTukar($KODE);
                    if ($data_tukar) {
                        $order = array(
                            "total" => count($data_tukar),
                            "tukar" => $data_tukar
                        );
                        $message = 'Data detail tukar poin pelanggan.';
                        $data = $order;
                        $status = 1;
                    } else {
                        $message = 'Data detail tukar poin tidak ditemukan.';
                        $data = null;
                        $status = 0;
                    }
                } else {
                    $message = 'Akun tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }

    public function getPembayaran() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->KODE) ? $KODE = $data->KODE : $KODE = '';
            isset($data->TGL_AWAL) ? $TGL_AWAL = $data->TGL_AWAL : $TGL_AWAL = '';
            isset($data->TGL_AKHIR) ? $TGL_AKHIR = $data->TGL_AKHIR : $TGL_AKHIR = '';
            isset($data->START) ? $START = $data->START : $START = '';
            isset($data->LIMIT) ? $LIMIT = $data->LIMIT : $LIMIT = '';


            if ($id_user != '' && $user_token != '') {
                //mengecek login user
                $is_login = $this->Apimodel->getUserByToken($id_user, $user_token);
                if ($is_login) {
                    $data_pembayaran = $this->Apimodel->getPembayaran($id_user, $KODE, $TGL_AWAL, $TGL_AKHIR, $START, $LIMIT);
                    // var_dump($data_kunjungan);
                    // exit();
                    if ($data_pembayaran) {
                        $pembayaran = array(
                            "total" => count($data_pembayaran),
                            "pembayaran" => $data_pembayaran
                        );
                        $message = 'Data Pembayaran.';
                        $data = $pembayaran;
                        $status = 1;
                    } else {
                        $message = 'Data pembayaran tidak ditemukan.';
                        $data = null;
                        $status = 0;
                    }
                } else {
                    $message = 'Akun tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }

    // --------------------------------------dari Mas Pii------------------------------------------
    
    public function getDetailPelunasan(){
        if($this->checkApp($this->input->get_request_header('appToken'))){
            $status     = -1;
            $message    = '';
            $data       = '';

            $data = (object)json_decode(file_get_contents('php://input'));
            
            isset($data->id_user)       ? $id_user      = $data->id_user        : $id_user      = '';
            isset($data->user_token)    ? $user_token   = $data->user_token     : $user_token   = '';
            
            isset($data->KODE)          ? $KODE         = $data->KODE           : $KODE         = '';

            if($id_user != '' && $user_token != '' && $KODE != ''){
                //mengecek login user
                $is_login   = $this->Apimodel->getUserByToken($id_user, $user_token);
                if($is_login){
                    $data_piutang = $this->Apimodel->getDetailPiutang($KODE);
                    if($data_piutang){
                        $order  = array(
                                "total"     => count($data_piutang),
                                "piutang"   => $data_piutang
                            );
                        $message    = 'Data detail Piutang.';
                        $data       = $order;
                        $status     = 1;
                    }else{
                        $message    = 'Data detail piutang tidak ditemukan.';
                        $data       = null;
                        $status     = 0;
                    }
                }else{
                    $message    = 'Akun tidak ditemukan.';
                    $data       = null;
                    $status     = 0;
                }
            }else{
                $message    = 'Field is empty.';
                $data       = null;
                $status     = 0;
            }
            $response   = array(
                    'status'    => $status,
                    'message'   => $message,
                    'data'      => $data
                );
            $this->makeOutput($response);
        }else{
            $this->jsonNoRespon();
        }
    }
    public function bayarPelunasan(){
        if($this->checkApp($this->input->get_request_header('appToken'))){
            $status     = -1;
            $message    = '';
            $data       = '';

            $data = (object)json_decode(file_get_contents('php://input'));
            
            isset($data->id_user)       ? $id_user      = $data->id_user        : $id_user      = '';
            isset($data->user_token)    ? $user_token   = $data->user_token     : $user_token   = '';
            
            isset($data->KODE)          ? $KODE         = $data->KODE           : $KODE         = '';
            isset($data->jumlah_bayar)  ? $jumlah_bayar = $data->jumlah_bayar   : $jumlah_bayar = '';
            isset($data->tgl_bayar)     ? $tgl_bayar    = $data->tgl_bayar      : $tgl_bayar    = '';
            isset($data->faktur_bayar)  ? $faktur_bayar = $data->faktur_bayar   : $faktur_bayar = '';

            if($id_user != '' && $user_token != '' && $KODE != '' && $jumlah_bayar != '' && $tgl_bayar != '' && $faktur_bayar != ''){
                //mengecek login user
                $is_login   = $this->Apimodel->getUserByToken($id_user, $user_token);
                if($is_login){
                    //untuk membuat id notif
                    $tanggal    = date('Y-m-d');
                    $tanggal_id = date('ymd');
                    $notif_ke   = ($this->Apimodel->totalNotifHariIni($tanggal_id)+1);

                    if($notif_ke<10){
                        $id_notif   = $tanggal_id.'000'.$notif_ke;
                    }else if($notif_ke>=10 && $notif_ke<100){
                        $id_notif   = $tanggal_id.'00'.$notif_ke;
                    }else if($notif_ke>=100 && $notif_ke<1000){
                        $id_notif   = $tanggal_id.'0'.$notif_ke;
                    }else{
                        $id_notif   = $tanggal_id.''.$notif_ke;
                    }

                    $kolektor   = $this->Apimodel->getKoletor($KODE);
                    $id_kolektor= $kolektor->ID_USER;

                    $data_notif = array(
                        'ID_NOTIF'      => $id_notif,
                        'TGL_NOTIF'     => $tanggal,
                        'NIK'           => $id_user,
                        'KODE_TOKO'     => $KODE,
                        'NO_FAKTUR'     => $faktur_bayar,
                        'TOTAL'         => $jumlah_bayar,
                        'TGL_PEMBAYARAN'=> $tgl_bayar,
                        'is_confirm'    => 1,
                        'KOLEKTOR'      => $id_kolektor,
                    );

                    //menyimpan notif ke database
                    $this->Apimodel->simpanNotifPembayaran($data_notif);

                    //mengirim notifikasi ke kolektor
                    $message    = $this->Apimodel->getDaftarPelunasan($id_kolektor, $KODE);

                    $msg = array(
                         'body' => $message,
                         'tipe' => 1,           //tipe 1 untuk notifikasi ketika ada pembayaran
                         'title' => "PUSH NOTIFICATION"
                    );

                    $registatoin_ids = array(
                        $kolektor->USER_TOKEN
                        );
                    if($message != null & $kolektor != null){
                        $this->sendNotificationKolektor($registatoin_ids, $msg);
                    }


                    $message    = 'Permintaan pembayaran berhasil disimpan.';
                    $data       = null;
                    $status     = 1;
                }else{
                    $message    = 'Akun tidak ditemukan.';
                    $data       = null;
                    $status     = 0;
                }
            }else{
                $message    = 'Field is empty.';
                $data       = null;
                $status     = 0;
            }
            $response   = array(
                    'status'    => $status,
                    'message'   => $message,
                    'data'      => $data
                );
            $this->makeOutput($response);
        }else{
            $this->jsonNoRespon();
        }
    }
    public function updateKegiatan() {
        //untuk mengecek apakah input dari aplikasi android
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->KODE) ? $KODE = $data->KODE : $KODE = '';
            isset($data->TGL_KUNJUNGAN) ? $TGL_KUNJUNGAN = $data->TGL_KUNJUNGAN : $TGL_KUNJUNGAN = '';
            isset($data->JAM_MULAI) ? $JAM_MULAI = $data->JAM_MULAI : $JAM_MULAI = '';
            isset($data->KEGIATAN) ? $KEGIATAN = $data->KEGIATAN : $KEGIATAN = '';


            if ($id_user != '' && $user_token != '' && $KODE != '' && $TGL_KUNJUNGAN != '' && $JAM_MULAI != '') {
                //mengecek login user
                $is_login = $this->Apimodel->getUserByToken($id_user, $user_token);
                if ($is_login) {
                    $jam_tutup = date('H:i:s');
                    $update_kunjungan = $this->Apimodel->updateKegiatan($id_user, $KODE, $TGL_KUNJUNGAN, $JAM_MULAI, $KEGIATAN);

                    if ($update_kunjungan) {
                        $message = "Data berhasil disimpan.";
                        $data = null;
                        $status = 1;
                    } else {
                        $message = "Tidak dapat mengubah data.";
                        $data = null;
                        $status = 0;
                    }
                } else {
                    $message = 'Akun tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }
    public function scantokotanpabarcode() {
        //untuk mengecek apakah input dari aplikasi android
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->KODE) ? $KODE = $data->KODE : $KODE = '';
            isset($data->LATITUDE) ? $LATITUDE = $data->LATITUDE : $LATITUDE = '';
            isset($data->LONGITUDE) ? $LONGITUDE = $data->LONGITUDE : $LONGITUDE = '';


            if ($id_user != '' && $user_token != '' && $KODE != '' && $LATITUDE != '' && $LONGITUDE != '') {
                //mengecek waktu scan, harus diantara jam 08.00 - 15.30
                $jam    = date('H');
                if($jam >= 8 && $jam <= 17){
                    $menit  = date('i');
                    if($jam == 15 && ($menit >= 30)){
                        //tidak bisa scan karena lebih dari jam setengah 4
                        $message = 'Maaf, transaksi pelanggan melebihi jam yang ditentukan (08:00 s/d 17:30)';
                        $data = null;
                        $status = 0;
                    }else{
                        //mengecek login user
                        $is_login = $this->Apimodel->getUserByToken($id_user, $user_token);
                        if ($is_login != false) {
                            //mengecek apakah ID Sales sesuai dengan toko atau tidak
                            $toko = $this->Apimodel->getTokobyKode($KODE);
                            $long = 0;
                            $lati = 0;

                            if (count($toko) == 1) {
                                if($toko[0]->KDSALES == $id_user){
                                    //mengecek apakah longitude dan latitude null
                                    if(is_null($toko[0]->LONGITUDE)|| is_null($toko[0]->LATITUDE)){
                                        //merubah koordinat toko, karena nilai awal null
                                        $LATITUDE   = (double)$LATITUDE;
                                        $LONGITUDE  = (double)$LONGITUDE;
                                        
                                        if($this->Apimodel->updateKoordinat($KODE, $LONGITUDE, $LATITUDE)){
                                            $datetime = date('Y-m-d H:i:s');
                                            $tanggal = substr($datetime, 0, 10);
                                            $jam_mulai = substr($datetime, 11);

                                            $dataKunjungan = $this->Apimodel->insertKunjungan($id_user, $KODE, $LATITUDE, $LONGITUDE, $tanggal, $jam_mulai, 0);

                                            if ($dataKunjungan) {
                                                $poinToko = $this->Apimodel->getPoin($KODE);
                                                $piutangToko = $this->Apimodel->getPiutang($KODE);
                                                if ($poinToko) {
                                                    $newdata = array(
                                                        'kunjungan' => $dataKunjungan,
                                                        'poin' => $poinToko,
                                                        'piutang' => $piutangToko,
                                                    );
                                                    $message = "Scan berhasil.";
                                                    $data = $newdata;
                                                    $status = 1;
                                                } else {
                                                    $message = "Data toko tidak ditemukan.";
                                                    $data = null;
                                                    $status = 0;
                                                }
                                            } else {
                                                $message = "Tidak dapat melakukan input data.";
                                                $data = null;
                                                $status = 0;
                                            }
                                        }else{
                                            $message = "Tidak dapat merubah koordinat.";
                                            $data = null;
                                            $status = 0;
                                        }
                                    }else{
                                        $long = (double)$toko[0]->LONGITUDE;
                                        $lati = (double)$toko[0]->LATITUDE;

                                        $LATITUDE   = (double)$LATITUDE;
                                        $LONGITUDE  = (double)$LONGITUDE;

                                        $jarak  = $this->hitungKoordinat($LATITUDE, $LONGITUDE, $lati, $long);
                                        
                                        if ($jarak < maksimalJarak) {
                                            //jika kurang dari 50 meter dapat melakukan kunjungan
                                            $datetime = date('Y-m-d H:i:s');
                                            $tanggal = substr($datetime, 0, 10);
                                            $jam_mulai = substr($datetime, 11);

                                            $dataKunjungan = $this->Apimodel->insertKunjungan($id_user, $KODE, $LATITUDE, $LONGITUDE, $tanggal, $jam_mulai, 0);

                                            if ($dataKunjungan) {
                                                $poinToko = $this->Apimodel->getPoin($KODE);
                                                $piutangToko = $this->Apimodel->getPiutang($KODE);
                                                if ($poinToko) {
                                                    $newdata = array(
                                                        'kunjungan' => $dataKunjungan,
                                                        'poin' => $poinToko,
                                                        'piutang' => $piutangToko,
                                                    );
                                                    $message = "Scan berhasil.";
                                                    $data = $newdata;
                                                    $status = 1;
                                                } else {
                                                    $message = "Data toko tidak ditemukan.";
                                                    $data = null;
                                                    $status = 0;
                                                }
                                            } else {
                                                $message = "Tidak dapat melakukan input data.";
                                                $data = null;
                                                $status = 0;
                                            }
                                        } else {
                                            //jika lebih dari 50 meter tidak dapat melakukan kunjungan
                                            $message = "Barcode yang discan tidak berada di lokasi pelanggan.";//" jarak : " . $jarak . ', maks : ';// . maksimalJarak;
                                            $data = null;
                                            $status = 0;
                                        }
                                    }
                                }else{
                                    $message = 'Maaf, barcode yang anda scan bukan pelanggan anda.';
                                    $data = null;
                                    $status = 0;
                                }
                            }else{
                                $message = 'Data Toko tidak ditemukan.';
                                $data = null;
                                $status = 0;
                            }
                        } else {
                            $message = 'Akun tidak ditemukan.';
                            $data = null;
                            $status = 0;
                        }
                    }
                }else{
                    $message = 'Maaf, transaksi pelanggan melebihi jam yang ditentukan (08:00 s/d 17:30)';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }
}


