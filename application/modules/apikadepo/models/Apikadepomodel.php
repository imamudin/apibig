<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Apikadepomodel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    public function getUserByToken($id_user, $user_token){
        $stat   = "select id_user, id_role, user_name, avatar, user_token from tb_user where id_user = '$id_user' and binary user_token = '$user_token' and id_role=6";

        $data = $this->db->query($stat);

        if($data->num_rows()==1){
            $data = $data->result();
            return $data[0];
        }else{
            return false;
        }
    }   

    public function login($username, $password){
        $stat   = "select id_user, id_role, user_name, avatar, user_token from tb_user where binary user_name = '$username' and binary password = '$password' and id_role=6";
        $data = $this->db->query($stat);

        if($data->num_rows()==1){
            return $data->result();
        }else{
            return false;
        }
    }
    public function updateToken($username, $password, $token){
        $query = "update tb_user set user_token='$token' 
                    where binary user_name='$username' and binary password = '$password' and id_role=6";
        if($this->db->query($query)){
            return true;
        }else{
            return false;
        }
    }
    public function getUser($username, $password){
        $stat   = "select id_user, id_role, user_name, avatar, user_token from tb_user where binary user_name = '$username' and binary password = '$password' and id_role=6";
        $data = $this->db->query($stat);

        if($data->num_rows()==1){
            return $data->result();
        }else{
            return false;
        }
    }

    public  function logout($id_user, $user_token){
        $sql    = "select * from tb_user where id_user='".$id_user."' or user_token='".$user_token."' and id_role=6";
        $data   = $this->db->query($sql);
        if($data->num_rows()==1){
            $data = $data->result();
            $data = $data[0];

            if($this->updateToken($data->USER_NAME, $data->PASSWORD, '')){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    public function insertKunjungan($id_user, $KODE, $latitude, $longitude, $tanggal, $jam_mulai){
        $stat   = "insert into tb_kunjungan (NIK, KODE, LATITUDE, LONGITUDE, TGL_KUNJUNGAN, JAM_MULAI) VALUES ('$id_user', '$KODE', '$latitude', '$longitude', '$tanggal', '$jam_mulai')";

        if($this->db->query($stat)){
            return $this->getKunjungan($id_user, $KODE, $tanggal, $jam_mulai);
        }else{
            return false;
        }
    }
    public function getKunjungan($id_user, $KODE, $tanggal, $jam_mulai){
        $stat   = "select * from tb_kunjungan where NIK = '$id_user' and KODE = '$KODE' and TGL_KUNJUNGAN='$tanggal' and JAM_MULAI='$jam_mulai'";

        $data = $this->db->query($stat);

        if($data->num_rows()==1){
            $data = $data->result();
            return $data[0];
        }else{
            return false;
        }
    }
    public function getPoin($KODE){
        $stat = "select b.KODE, NAMA, ALAMAT, b.PIMPINAN, b.TELEPON, b.REALPLAFON, b.KDSALES, b.JATUHTEMPOBAYAR, b.LATITUDE, b.LONGITUDE, ifnull(count(*),0) JUMLAH_ORDER , ifnull(sum(JUMLAH),0) JUMLAH_BELI, ifnull(sum(a.poin),0) TOTAL_POIN, 
                ifnull((select ifnull(sum(c.POIN),0) from tb_tukar_poin c where c.id_toko = b.kode),0) TUKAR,
                ifnull(sum(a.poin) - (select ifnull(sum(c.POIN),0) from tb_tukar_poin c where c.id_toko = b.kode),0) POIN
                    from  tb_toko b left outer join tb_poin_toko a on b.KODE = a.ID_TOKO 
                    where b.KODE = '$KODE'
                 group by a.ID_TOKO
                order by POIN desc";
        // echo $stat;
        // exit();
        $data = $this->db->query($stat);
        if($data->num_rows()==1){
            $data = $data->result();
            return $data[0];
        }else{
            return false;
        }
    }
    public function getPiutang($KODE){
//        $stat = "select b.REALPLAFON, ifnull(TOTAL_PIUTANG, 0) TOTAL_PIUTANG, ifnull(PIUTANG, 0) TAGIHAN_TERDEKAT, TGL_JATUH_TEMPO
//                from  tb_toko b 
//                left outer join 
//                        (select b.KODE, sum(total) PIUTANG, 
//                                                date_format(min(a.TGL_JATUH_TEMPO), '%d-%m-%Y') TGL_JATUH_TEMPO, date_format(a.TGL_JATUH_TEMPO, '%Y-%m-%d') TGL
//                                                from tb_order a, tb_toko b, tb_user_toko c
//                                                where a.KODE_TOKO = b.KODE and is_approval = 1
//                                                and no_faktur not in (select x.no_faktur from tb_pembayaran_toko x where a.NO_FAKTUR = x.NO_FAKTUR)
//                                                and b.KODE = c.ID_USER and binary c.ID_USER = '$KODE'
//                                                group by b.KODE
//                                                union all
//                                            select b.KODE,  sum(a.total) - sum(x.TOTAL) PIUTANG,
//                                                date_format(min(a.TGL_JATUH_TEMPO), '%d-%m-%Y') TGL_JATUH_TEMPO, date_format(a.TGL_JATUH_TEMPO, '%Y-%m-%d') TGL
//                                                from tb_order a left outer join tb_pembayaran_toko x on a.NO_FAKTUR = x.NO_FAKTUR, tb_toko b, tb_user_toko c
//                                                where a.KODE_TOKO = b.KODE  and is_approval = 1
//                                                and a.TOTAL > (select sum(x.TOTAL) from tb_pembayaran_toko x where a.NO_FAKTUR = x.NO_FAKTUR)
//                                                and b.KODE = c.ID_USER and binary c.ID_USER = '$KODE'
//                                                group by b.KODE
//                                                order by TGL
//                                                limit 0,1) X on b.KODE = X.KODE
//                         left outer join 
//                        (select KODE,  sum(PIUTANG) TOTAL_PIUTANG from (
//                                            select b.KODE,  sum(total) PIUTANG
//                                                from tb_order a, tb_toko b, tb_user_toko c
//                                                where a.KODE_TOKO = b.KODE and is_approval = 1
//                                                and no_faktur not in (select x.no_faktur from tb_pembayaran_toko x where a.NO_FAKTUR = x.NO_FAKTUR)
//                                                and b.KODE = c.ID_USER and binary c.ID_USER = '$KODE'
//                                                group by b.KODE
//                                                union all
//                                            select b.KODE,  sum(a.total) - sum(x.TOTAL) PIUTANG
//                                                from tb_order a left outer join tb_pembayaran_toko x on a.NO_FAKTUR = x.NO_FAKTUR, tb_toko b, tb_user_toko c
//                                                where a.KODE_TOKO = b.KODE  and is_approval = 1
//                                                and a.TOTAL > (select sum(x.TOTAL) from tb_pembayaran_toko x where a.NO_FAKTUR = x.NO_FAKTUR)
//                                                and b.KODE = c.ID_USER and binary c.ID_USER = '$KODE'
//                                                group by b.KODE) x
//                                            group by KODE) Y on b.KODE = Y.KODE
//                where b.KODE = '$KODE'";

        $stat = "select b.REALPLAFON, ifnull(TOTAL_PIUTANG, 0) TOTAL_PIUTANG, ifnull(PIUTANG, 0) TAGIHAN_TERDEKAT, TGL_JATUH_TEMPO
                from  tb_toko b 
                left outer join 
                        (select b.KODE, sum(jumlah) PIUTANG, 
                                                date_format(min(a.TGL_JATUH_TEMPO), '%d-%m-%Y') TGL_JATUH_TEMPO, date_format(a.TGL_JATUH_TEMPO, '%Y-%m-%d') TGL
                                                from tb_penjualan a, tb_toko b, tb_user_toko c
                                                where a.K_PLG = b.KODE
                                                and faktur not in (select x.no_faktur from tb_pembayaran_toko x where a.FAKTUR = x.NO_FAKTUR)
                                                and b.KODE = c.ID_USER and binary c.ID_USER = '$KODE'
                                                group by b.KODE
                                                union all
                                            select b.KODE,  sum(a.jumlah) - sum(x.TOTAL) PIUTANG,
                                                date_format(min(a.TGL_JATUH_TEMPO), '%d-%m-%Y') TGL_JATUH_TEMPO, date_format(a.TGL_JATUH_TEMPO, '%Y-%m-%d') TGL
                                                from tb_penjualan a left outer join tb_pembayaran_toko x on a.FAKTUR = x.NO_FAKTUR, tb_toko b, tb_user_toko c
                                                where a.K_PLG = b.KODE
                                                and a.JUMLAH > (select sum(x.TOTAL) from tb_pembayaran_toko x where a.FAKTUR = x.NO_FAKTUR)
                                                and b.KODE = c.ID_USER and binary c.ID_USER = '$KODE'
                                                group by b.KODE
                                                order by TGL
                                                limit 0,1) X on b.KODE = X.KODE
                         left outer join 
                        (select KODE,  sum(PIUTANG) TOTAL_PIUTANG from (
                                            select b.KODE,  sum(jumlah) PIUTANG
                                                from tb_penjualan a, tb_toko b, tb_user_toko c
                                                where a.K_PLG = b.KODE
                                                and faktur not in (select x.no_faktur from tb_pembayaran_toko x where a.FAKTUR = x.NO_FAKTUR)
                                                and b.KODE = c.ID_USER and binary c.ID_USER = '$KODE'
                                                group by b.KODE
                                                union all
                                            select b.KODE,  sum(a.jumlah) - sum(x.TOTAL) PIUTANG
                                                from tb_penjualan a left outer join tb_pembayaran_toko x on a.FAKTUR = x.NO_FAKTUR, tb_toko b, tb_user_toko c
                                                where a.K_PLG = b.KODE
                                                and a.JUMLAH > (select sum(x.TOTAL) from tb_pembayaran_toko x where a.FAKTUR = x.NO_FAKTUR)
                                                and b.KODE = c.ID_USER and binary c.ID_USER = '$KODE'
                                                group by b.KODE) x
                                            group by KODE) Y on b.KODE = Y.KODE
                where b.KODE = '$KODE'";
        
        $data = $this->db->query($stat);

        if($data->num_rows()==1){
            $data = $data->result();
            return $data[0];
        }else{
            return array(
                'PIUTANG'           => 0,
                'TGL_JATUH_TEMPO'   => '-'
            );
        }
    }

    public function updateKunjungan($id_user, $KODE, $TGL_KUNJUNGAN, $JAM_MULAI,$jam_tutup){
        $query = "update tb_kunjungan set JAM_SELESAI='$jam_tutup' 
                    where NIK='$id_user' and  KODE = '$KODE' and TGL_KUNJUNGAN='$TGL_KUNJUNGAN' and JAM_MULAI='$JAM_MULAI'";
        if($this->db->query($query)){
            return true;
        }else{
            return false;
        }
    }
    public function hapus_stok($KODE,$tanggal){
        $stat   = "delete from tb_stok_toko where ID_TOKO = '$KODE' and TGL_MUTASI = '$tanggal'";
        $data = $this->db->query($stat);

        if($data){
            return true;
        }else{
            return false;
        }
    }
    public function updateStok($KODE,$tanggal, $jumlah_stok){
        $query = "update tb_stok_toko set STOK='$jumlah_stok' 
                    where KODE = '$KODE' and TGL_MUTASI = '$tanggal'";
        if($this->db->query($query)){
            return true;
        }else{
            return false;
        }
    }
    public function insertStok($KODE, $id_produk, $tanggal, $is_masuk, $in_order, $jumlah_stok){
        $stat   = "insert into tb_stok_toko (ID_TOKO, ID_PRODUK, TGL_MUTASI, IS_MASUK, ID_ORDER, STOK) VALUES ('$KODE', '$id_produk', '$tanggal', '$is_masuk', '$in_order', '$jumlah_stok')";

        if($this->db->query($stat)){
            return true;
        }else{
            return false;
        }
    }
    public function getProduk(){
        $stat   = "select * from tb_produk";
        $data = $this->db->query($stat);
        if($data->num_rows()>=1){
            $data = $data->result();
            return $data;
        }else{
            return false;
        }
    }
    public function getProdukNonSemen(){
        $stat   = "select * from tb_non_semen";
        $data = $this->db->query($stat);
        if($data->num_rows()>=1){
            $data = $data->result();
            return $data;
        }else{
            return false;
        }
    }
    public function getHarga(){
        $stat   = "select a.ID_PRODUK, a.JUMLAH, a.HARGA, b.NAMA_PRODUK from tb_harga a, tb_produk b where a.id_produk=b.id_produk and a.jumlah=1 order by id_produk";
        $data = $this->db->query($stat);
        if($data->num_rows()>=1){
            $data = $data->result();
            return $data;
        }else{
            return false;
        }
    }
    public function getGudang(){
        $stat   = "select KODE, NAMA from tb_gudang";
        $data = $this->db->query($stat);

        if($data->num_rows()>=1){
            $data = $data->result();
            return $data;
        }else{
            return false;
        }
    }
    public function totalOrderHariIni($tanggal_id){
        $sql    ="select ifnull(count(*), 0) as total from tb_order where ID_ORDER like '$tanggal_id%'";

        $result = $this->db->query($sql)->result();

        return $result[0]->total;
    }
    public function totalOrderHariIniNonSemen($tanggal_id){
        $sql    ="select ifnull(count(*), 0) as total from tb_order_non_semen where ID_ORDER like '$tanggal_id%'";

        $result = $this->db->query($sql)->result();

        return $result[0]->total;
    }
    public function insertorder($id_order, $id_user, $KODE, $faktur, $tgl_faktur, $total_harga, $KODE_GUDANG, $tgl_jth_tempo, $tanggal_kirim, $JENIS_PENGIRIMAN, $is_approval){
        $stat   = "insert into tb_order (ID_ORDER, NIK, KODE_TOKO, NO_FAKTUR, TGL_FAKTUR, TOTAL, KODE_GUDANG, TGL_JATUH_TEMPO, TGL_KIRIM, JENIS_PENGIRIMAN, is_approval) VALUES ('$id_order', '$id_user', '$KODE', '$faktur', '$tgl_faktur', '$total_harga', '$KODE_GUDANG', '$tgl_jth_tempo', '$tanggal_kirim', '$JENIS_PENGIRIMAN', $is_approval)";

        if($this->db->query($stat)){
            return true;
        }else{
            return false;
        }
    }
    public function insertorderNonSemen($id_order, $id_user, $KODE, $faktur, $tgl_faktur, $total_harga, $KODE_GUDANG, $tgl_jth_tempo, $tanggal_kirim, $JENIS_PENGIRIMAN, $is_approval){
        $stat   = "insert into tb_order_non_semen (ID_ORDER, NIK, KODE_TOKO, NO_FAKTUR, TGL_FAKTUR, TOTAL, KODE_GUDANG, TGL_JATUH_TEMPO, TGL_KIRIM, JENIS_PENGIRIMAN, is_approval) VALUES ('$id_order', '$id_user', '$KODE', '$faktur', '$tgl_faktur', '$total_harga', '$KODE_GUDANG', '$tgl_jth_tempo', '$tanggal_kirim', '$JENIS_PENGIRIMAN', $is_approval)";

        if($this->db->query($stat)){
            return true;
        }else{
            return false;
        }
    }
    public function getPoinPerPembelian(){
        $sql    ="select jumlah_pembelian as poin from tb_poin";

        $result = $this->db->query($sql)->result();

        return $result[0]->poin;
    }
    public function insertPoin($KODE,$id_order, $jumlah_pesanan, $poin){
        $stat   = "insert into tb_poin_toko (ID_TOKO, ID_ORDER, JUMLAH, POIN) VALUES ('$KODE', '$id_order', '$jumlah_pesanan', '$poin')";
        if($this->db->query($stat)){
            return true;
        }else{
            return false;
        }
    }

    public function cekFaktur($id_toko){
//        $stat   = "select total, a.TGL_JATUH_TEMPO, datediff(a.TGL_JATUH_TEMPO, now()) SISA_HARI_JATUH_TEMPO
//                from tb_order a, tb_toko b
//                where a.KODE_TOKO = b.KODE and b.KODE = '$id_toko' and is_approval = 1
//                and a.NO_FAKTUR not in (select x.NO_FAKTUR from tb_pembayaran_toko x where a.KODE_TOKO = x.PELANGGAN)
//                order by TGL_JATUH_TEMPO
//                limit 0,1";
        
        $stat   = "select jumlah total, a.TGL_JATUH_TEMPO, datediff(a.TGL_JATUH_TEMPO, now()) SISA_HARI_JATUH_TEMPO
                from tb_penjualan a, tb_toko b
                where a.K_PLG = b.KODE and b.KODE = '$id_toko'
                and a.FAKTUR not in (select x.NO_FAKTUR from tb_pembayaran_toko x where a.K_PLG = x.PELANGGAN)
                order by TGL_JATUH_TEMPO
                limit 0,1";

        $data = $this->db->query($stat);

        if($data->num_rows()==1){
            $data = $data->result();
            $data = $data[0];
            if($data->SISA_HARI_JATUH_TEMPO > 0){
                return true;
            }else{
                return false;
            }
        }else{
            return true;
        }
    }
    public function getTotalPiutang($KODE){
//        $stat = "select KODE, NAMA, ALAMAT, (sum(tagihan) - sum(pembayaran)) PIUTANG, (sum(faktur_a)-sum(faktur_b)) JUMLAH_FAKTUR, date_format(TGL_JATUH_TEMPO, '%d-%m-%Y') TGL_JATUH_TEMPO from (
//            select b.KODE, b.NAMA, b.ALAMAT, sum(total) tagihan, 0 pembayaran, count(*) faktur_a, 0 faktur_b, min(a.TGL_JATUH_TEMPO) TGL_JATUH_TEMPO
//            from tb_order a, tb_toko b
//            where a.KODE_TOKO = b.KODE and is_approval = 1
//            group by b.KODE
//            union all
//            select b.KODE, b.NAMA, b.ALAMAT, 0 tagihan, sum(TOTAL)  pembayaran, 0 faktur_a, count(*) faktur_b, '' TGL_JATUH_TEMPO
//            from tb_pembayaran_toko a, tb_toko b
//            where a.PELANGGAN = b.KODE
//            group by b.KODE
//            ) x
//            group by NAMA
//            having KODE = '$KODE'
//            order by TGL_JATUH_TEMPO desc";
        
        $stat = "select KODE, NAMA, ALAMAT, (sum(tagihan) - sum(pembayaran)) PIUTANG, (sum(faktur_a)-sum(faktur_b)) JUMLAH_FAKTUR, date_format(TGL_JATUH_TEMPO, '%d-%m-%Y') TGL_JATUH_TEMPO from (
            select b.KODE, b.NAMA, b.ALAMAT, sum(jumlah) tagihan, 0 pembayaran, count(*) faktur_a, 0 faktur_b, min(a.TGL_JATUH_TEMPO) TGL_JATUH_TEMPO
            from tb_penjualan a, tb_toko b
            where a.K_PLG = b.KODE
            group by b.KODE
            union all
            select b.KODE, b.NAMA, b.ALAMAT, 0 tagihan, sum(TOTAL)  pembayaran, 0 faktur_a, count(*) faktur_b, '' TGL_JATUH_TEMPO
            from tb_pembayaran_toko a, tb_toko b
            where a.PELANGGAN = b.KODE
            group by b.KODE
            ) x
            group by NAMA
            having KODE = '$KODE'
            order by TGL_JATUH_TEMPO desc";
        $data = $this->db->query($stat);
        if($data->num_rows()==1){
            $data = $data->result();
            return $data[0]->PIUTANG;
        }else{
            return 0;
        }
    }
    public function getJatuhTempo($KODE){
        $stat = "SELECT a.JATUHTEMPOBAYAR
                FROM tb_toko a
                where a.KODE='$KODE'";
        $data = $this->db->query($stat);
        if($data->num_rows()==1){
            $data = $data->result();
            return $data[0]->JATUHTEMPOBAYAR;
        }else{
            return 0;
        }
    }
    public function getNIKSales($KODE){
        $stat = "SELECT a.KDSALES
                FROM tb_toko a
                where a.KODE='$KODE'";
        $data = $this->db->query($stat);
        if($data->num_rows()==1){
            $data = $data->result();
            return $data[0]->KDSALES;
        }else{
            return 0;
        }
    }
    public function insertDetailOrder($id_order, $ID_PRODUK, $jumlah_pesanan, $HARGA, $jumlah_HARGA){
        $stat   = "insert into tb_order_detail (ID_ORDER, ID_PRODUK, JUMLAH, HARGA, JUMLAH_HARGA) VALUES ('$id_order', '$ID_PRODUK', '$jumlah_pesanan', '$HARGA', '$jumlah_HARGA')";

        if($this->db->query($stat)){
            return true;
        }else{
            return false;
        }
    }
    public function insertDetailOrderNonSemen($id_order, $ID_PRODUK, $jumlah_pesanan, $HARGA, $jumlah_HARGA){
        $stat   = "insert into tb_order_non_semen_detail (ID_ORDER, ID_PRODUK, JUMLAH, HARGA, JUMLAH_HARGA) VALUES ('$id_order', '$ID_PRODUK', '$jumlah_pesanan', '$HARGA', '$jumlah_HARGA')";

        if($this->db->query($stat)){
            return true;
        }else{
            return false;
        }
    }
    public function getDefaultGudang($id_user){
        $stat = "SELECT a.UNIT
                FROM tb_pegawai a
                where a.nik='$id_user'";
        $data = $this->db->query($stat);
        if($data->num_rows()==1){
            $data = $data->result();
            return $data[0]->UNIT;
        }else{
            return "";
        }
    }
    public function getTokobyKode($KODE){
        $stat = "SELECT *
                FROM tb_toko a
                where a.KODE='$KODE'
                limit 0,1";
        $data = $this->db->query($stat);
        if($data->num_rows()==1){
            $data = $data->result();
            return $data;
        }else{
            return array();
        }
    }
    public function getKunjungans($id_user, $KODE, $TGL_AWAL, $TGL_AKHIR, $start, $limit){
        $sqladd     = "";
        
        if($start == ''){
            $start = 0;
        }
        if($limit == ''){
            $limit = 25;
        }

        //untuk menambahkan custom query
        if($KODE != ''){
            $sqladd .= " and c.KODE = " . $this->db->escape($KODE) . " ";
        }
        if (strlen($TGL_AWAL) > 0 && strlen($TGL_AKHIR) > 0) {
            $sqladd .= " and tgl_kunjungan BETWEEN " . $this->db->escape($TGL_AWAL) . " and " . $this->db->escape($TGL_AKHIR) . "";
        }

        $sql = "SELECT b.NIK, b.NAMA NAMA_PEGAWAI, c.KODE, c.NAMA NAMA_TOKO, date_format(tgl_kunjungan, '%d-%m-%Y') TGL_KUNJUNGAN, 
                TIME_FORMAT(JAM_MULAI, '%H:%i') JAM_MULAI, TIME_FORMAT(JAM_SELESAI, '%H:%i') JAM_SELESAI, 
                concat(TIME_FORMAT(TIMEDIFF(JAM_SELESAI,JAM_MULAI), '%H')*60 + TIME_FORMAT(TIMEDIFF(JAM_SELESAI,JAM_MULAI), '%i'), ' menit') DURASI 
                FROM tb_kunjungan a, tb_pegawai b, tb_toko c
                where a.nik = b.nik and a.kode = c.kode
                and b.nik = c.kdsales
                and a.nik = '$id_user'
                " . $sqladd . "
                 order by tgl_kunjungan desc, JAM_MULAI desc
                 limit $start , $limit";

        $data = $this->db->query($sql);
        if($data->num_rows()>=0){
            return $data->result();
        }else{
            return false;
        }
    }
    public function getOrders($id_user, $KODE, $TGL_AWAL, $TGL_AKHIR, $start, $limit){
        $sqladd     = "";
        
        if($start == ''){
            $start = 0;
        }
        if($limit == ''){
            $limit = maksimalRequest;
        }

        //untuk menambahkan custom query
        if($KODE != ''){
            $sqladd .= " and c.KODE = " . $this->db->escape($KODE) . " ";
        }
        if (strlen($TGL_AWAL) > 0 && strlen($TGL_AKHIR) > 0) {
            $sqladd .= " and tgl_faktur BETWEEN " . $this->db->escape($TGL_AWAL) . " and " . $this->db->escape($TGL_AKHIR) . "";
        }

        $sql = "SELECT date_format(tgl_faktur, '%d-%m-%Y') TGL_FAKTUR, a.ID_ORDER, a.NO_FAKTUR, c.KODE, c.NAMA NAMA_TOKO, 
                a.TOTAL,  date_format(TGL_JATUH_TEMPO, '%d-%m-%Y') TGL_JATUH_TEMPO,
                date_format(TGL_KIRIM, '%d-%m-%Y') TGL_KIRIM,  b.NAMA NAMA_PEGAWAI, d.NAMA NAMA_GUDANG, a.ID_ORDER
                FROM tb_order a,  tb_pegawai b, (select t.KODE, t.NAMA, t.ALAMAT, t.KOTA, sales.NAMA as NAMA_SALES, sales.NIK, t.KDSALES
                                                        FROM tb_toko t, (SELECT p.*
                                                                        FROM tb_pegawai p, (select * 
                                                                                            from tb_pegawai tb_p
                                                                                            where tb_p.NIK='$id_user') kadepo
                                                                        WHERE kadepo.unit = p.unit
                                                                        AND p.JOBDESK='SALES') sales
                                                        where t.KDSALES=sales.NIK) c, tb_gudang d
                where a.nik = b.nik and a.kode_toko = c.kode and a.KODE_GUDANG = d.KODE
                and b.nik = c.kdsales and is_approval = 1
                " . $sqladd . "
                 order by tgl_faktur desc, c.NAMA desc
                 limit $start , $limit";

        $data = $this->db->query($sql);
        if($data->num_rows()>=0){
            return $data->result();
        }else{
            return false;
        }
    }
    public function getOrdersNonSemen($id_user, $KODE, $TGL_AWAL, $TGL_AKHIR, $start, $limit){
        $sqladd     = "";
        
        if($start == ''){
            $start = 0;
        }
        if($limit == ''){
            $limit = maksimalRequest;
        }

        //untuk menambahkan custom query
        if($KODE != ''){
            $sqladd .= " and c.KODE = " . $this->db->escape($KODE) . " ";
        }
        if (strlen($TGL_AWAL) > 0 && strlen($TGL_AKHIR) > 0) {
            $sqladd .= " and tgl_faktur BETWEEN " . $this->db->escape($TGL_AWAL) . " and " . $this->db->escape($TGL_AKHIR) . "";
        }

        $sql = "SELECT date_format(tgl_faktur, '%d-%m-%Y') TGL_FAKTUR, a.ID_ORDER, a.NO_FAKTUR, c.KODE, c.NAMA NAMA_TOKO, 
                a.TOTAL,  date_format(TGL_JATUH_TEMPO, '%d-%m-%Y') TGL_JATUH_TEMPO,
                date_format(TGL_KIRIM, '%d-%m-%Y') TGL_KIRIM,  b.NAMA NAMA_PEGAWAI, d.NAMA NAMA_GUDANG, a.ID_ORDER
                FROM tb_order_non_semen a,  tb_pegawai b, (select t.KODE, t.NAMA, t.ALAMAT, t.KOTA, sales.NAMA as NAMA_SALES, sales.NIK, t.KDSALES
                                                        FROM tb_toko t, (SELECT p.*
                                                                        FROM tb_pegawai p, (select * 
                                                                                            from tb_pegawai tb_p
                                                                                            where tb_p.NIK='$id_user') kadepo
                                                                        WHERE kadepo.unit = p.unit
                                                                        AND p.JOBDESK='SALES') sales
                                                        where t.KDSALES=sales.NIK) c, tb_gudang d
                where a.nik = b.nik and a.kode_toko = c.kode and a.KODE_GUDANG = d.KODE
                and b.nik = c.kdsales and is_approval = 1
                " . $sqladd . "
                 order by tgl_faktur desc, c.NAMA desc
                 limit $start , $limit";

        $data = $this->db->query($sql);
        if($data->num_rows()>=0){
            return $data->result();
        }else{
            return false;
        }
    }
    public function getDetailOrders($id_order){
        $sql = "select a.NO_FAKTUR, date_format(tgl_faktur, '%d-%m-%Y') TGL_FAKTUR, 
                        c.NAMA_PRODUK, b.JUMLAH, b.HARGA, b.JUMLAH_HARGA
                from tb_order a, tb_order_detail b, tb_produk c
                where a.id_order = b.id_order and b.id_produk = c.id_produk and is_approval = 1
                and b.id_order = " . $this->db->escape($id_order) . "
                order by b.id_produk desc";

        $data = $this->db->query($sql);
        if($data->num_rows()>=0){
            return $data->result();
        }else{
            return false;
        }
    }
    public function getDetailOrdersNonSemen($id_order){
        $sql = "select a.NO_FAKTUR, date_format(tgl_faktur, '%d-%m-%Y') TGL_FAKTUR, 
                        c.NAMA_PRODUK, b.JUMLAH, b.HARGA, b.JUMLAH_HARGA
                from tb_order_non_semen a, tb_order_non_semen_detail b, tb_non_semen c
                where a.id_order = b.id_order and b.id_produk = c.id_produk and is_approval = 1
                and b.id_order = " . $this->db->escape($id_order) . "
                order by b.id_produk desc";

        $data = $this->db->query($sql);
        if($data->num_rows()>=0){
            return $data->result();
        }else{
            return false;
        }
    }
    public function getToko($id_user, $start, $limit){
        if($start == ''){
            $start = 0;
        }
        if($limit == ''){
            $limit = maksimalRequest;
        }
        
        $sql = "select t.KODE, t.NAMA, t.ALAMAT, t.KOTA, sales.NAMA as NAMA_SALES, sales.NIK
                FROM tb_toko t, (SELECT p.*
                                FROM tb_pegawai p, (select * 
                                                    from tb_pegawai tb_p
                                                    where tb_p.NIK='$id_user') kadepo
                                WHERE kadepo.unit = p.unit
                                AND p.JOBDESK='SALES') sales
                where t.KDSALES=sales.NIK
                ORDER BY t.NAMA
                limit $start, $limit";

        $data = $this->db->query($sql);
        if($data->num_rows()>=0){
            return $data->result();
        }else{
            return false;
        }
    }
    public function checkTokoByKadepo($id_kadepo, $KODE){
        $sql = "select t.KODE, t.NAMA, t.ALAMAT, t.KOTA, sales.NAMA as NAMA_SALES, sales.NIK
                FROM tb_toko t, (SELECT p.*
                                FROM tb_pegawai p, (select * 
                                                    from tb_pegawai tb_p
                                                    where tb_p.NIK='$id_kadepo') kadepo
                                WHERE kadepo.unit = p.unit
                                AND p.JOBDESK='SALES') sales
                where t.KDSALES=sales.NIK
                and t.KODE='$KODE'
                ORDER BY t.NAMA
                limit 0, 1";

        $data = $this->db->query($sql);
        if($data->num_rows()>=0){
            return $data->result();
        }else{
            return false;
        }
    }
    public function getPiutangs($id_user, $KODE, $start, $limit){
        $sqladd     = "";
        
        if($start == ''){
            $start = 0;
        }
        if($limit == ''){
            $limit = maksimalRequest;
        }

        //untuk menambahkan custom query
        if($KODE != ''){
            $sqladd .= " having KODE = " . $this->db->escape($KODE) . " ";
        }

        $sql = "select KODE, NAMA, ALAMAT, (sum(tagihan) - sum(pembayaran)) PIUTANG, 
                (sum(faktur_a)-sum(faktur_b)) JUMLAH_FAKTUR, 
                case when sum(tagihan) - sum(pembayaran) > 0 then date_format(TGL_JATUH_TEMPO, '%d-%m-%Y')
                else '-' end TGL_JATUH_TEMPO 
                from (
                select b.KODE, b.NAMA, b.ALAMAT, sum(jumlah) tagihan, 0 pembayaran, count(*) faktur_a, 0 faktur_b, min(a.TGL_JATUH_TEMPO) TGL_JATUH_TEMPO
                from tb_penjualan a, (select t.KODE, t.NAMA, t.ALAMAT, t.KOTA, sales.NAMA as NAMA_SALES, sales.NIK
                                                        FROM tb_toko t, (SELECT p.*
                                                                        FROM tb_pegawai p, (select * 
                                                                                            from tb_pegawai tb_p
                                                                                            where tb_p.NIK='$id_user') kadepo
                                                                        WHERE kadepo.unit = p.unit
                                                                        AND p.JOBDESK='SALES') sales
                                                        where t.KDSALES=sales.NIK) b
                where a.K_PLG = b.KODE
                group by b.KODE
                union all
                select b.KODE, b.NAMA, b.ALAMAT, 0 tagihan, sum(TOTAL)  pembayaran, 0 faktur_a, count(*) faktur_b, '' TGL_JATUH_TEMPO
                from tb_pembayaran_toko a, (select t.KODE, t.NAMA, t.ALAMAT, t.KOTA, sales.NAMA as NAMA_SALES, sales.NIK
                                                        FROM tb_toko t, (SELECT p.*
                                                                        FROM tb_pegawai p, (select * 
                                                                                            from tb_pegawai tb_p
                                                                                            where tb_p.NIK='$id_user') kadepo
                                                                        WHERE kadepo.unit = p.unit
                                                                        AND p.JOBDESK='SALES') sales
                                                        where t.KDSALES=sales.NIK) b
                where a.PELANGGAN = b.KODE
                group by b.KODE
                ) x
                group by NAMA
                " . $sqladd . "
                order by date_format(TGL_JATUH_TEMPO, '%Y-%m-%d')
                limit $start , $limit ";

        $data = $this->db->query($sql);
        if($data->num_rows()>=0){
            return $data->result();
        }else{
            return false;
        }
    }
    public function getDetailPiutang($KODE){
        //b.NAMA, b.ALAMAT,
//        $sql = "select KODE, NO_FAKTUR, TGL_FAKTUR, TOTAL, PIUTANG, TGL_JATUH_TEMPO from (
//                select b.KODE, b.NAMA, b.ALAMAT, a.NO_FAKTUR, date_format(a.TGL_FAKTUR, '%d-%m-%Y') TGL_FAKTUR, a.TOTAL, (a.total - sum(x.TOTAL)) PIUTANG,  
//                        date_format(a.TGL_JATUH_TEMPO, '%d-%m-%Y') TGL_JATUH_TEMPO
//                        from tb_order a left outer join tb_pembayaran_toko x on a.NO_FAKTUR = x.NO_FAKTUR, tb_toko b
//                        where a.KODE_TOKO = b.KODE  and is_approval = 1
//                        and a.TOTAL > (select sum(z.TOTAL) from tb_pembayaran_toko z where a.NO_FAKTUR = z.NO_FAKTUR)
//                 union all
//                 select b.KODE, b.NAMA, b.ALAMAT, a.NO_FAKTUR, date_format(a.TGL_FAKTUR, '%d-%m-%Y') TGL_FAKTUR, a.TOTAL, a.total PIUTANG,  
//                        date_format(a.TGL_JATUH_TEMPO, '%d-%m-%Y') TGL_JATUH_TEMPO
//                        from tb_order a, tb_toko b
//                        where a.KODE_TOKO = b.KODE and is_approval = 1
//                        and no_faktur not in (select x.no_faktur from tb_pembayaran_toko x where a.NO_FAKTUR = x.NO_FAKTUR)
//                 ) x 
//                     where KODE = " . $this->db->escape($KODE) . "";
        
        
        $sql = "select KODE, NO_FAKTUR, TGL_FAKTUR, TOTAL, PIUTANG, TGL_JATUH_TEMPO from (
                select b.KODE, b.NAMA, b.ALAMAT, a.FAKTUR NO_FAKTUR, date_format(a.TANGGAL, '%d-%m-%Y') TGL_FAKTUR, a.JUMLAH TOTAL, (a.jumlah - sum(x.TOTAL)) PIUTANG,  
                        date_format(a.TGL_JATUH_TEMPO, '%d-%m-%Y') TGL_JATUH_TEMPO
                        from tb_penjualan a left outer join tb_pembayaran_toko x on a.FAKTUR = x.NO_FAKTUR, tb_toko b
                        where a.K_PLG = b.KODE 
                        and a.JUMLAH > (select sum(z.TOTAL) from tb_pembayaran_toko z where a.FAKTUR = z.NO_FAKTUR)
                 union all
                 select b.KODE, b.NAMA, b.ALAMAT, a.FAKTUR NO_FAKTUR, date_format(a.TANGGAL, '%d-%m-%Y') TGL_FAKTUR, a.JUMLAH TOTAL, a.jumlah PIUTANG,  
                        date_format(a.TGL_JATUH_TEMPO, '%d-%m-%Y') TGL_JATUH_TEMPO
                        from tb_penjualan a, tb_toko b
                        where a.K_PLG = b.KODE
                        and faktur not in (select x.no_faktur from tb_pembayaran_toko x where a.FAKTUR = x.NO_FAKTUR)
                 ) x 
                     where KODE = " . $this->db->escape($KODE) . "";

        $data = $this->db->query($sql);
        if($data->num_rows()>=0){
            return $data->result();
        }else{
            return false;
        }
    }
    public function getKadepo($id_user){
        $sql = "select u.ID_USER, u.USER_TOKEN, p.NAMA, p.UNIT, g.NAMA as NAMA_GUDANG, g.ALAMAT as ALAMAT_GUDANG
                from tb_user u, tb_pegawai p, tb_gudang g
                where u.ID_USER=p.NIK
                and g.KODE=p.UNIT
                and u.ID_USER='$id_user'
                ";
        $data = $this->db->query($sql);
        if($data->num_rows()>=1){
            $data = $data->result();
            return $data[0];
        }else{
            return false;
        }
    }
    public function getTotalFakturBulanIni($setengah_faktur){
        $sql    ="select ifnull(count(*), 0) as total from tb_order where NO_FAKTUR like '$setengah_faktur%' and is_approval = 1";

        $result = $this->db->query($sql)->result();

        return $result[0]->total;
    }

    // ---------------------------------------------dari Mas Pii-----------------------------------------------
    public function getPoinPelanggan($id_user, $KODE, $start, $limit){
        $sqladd     = "";
        
        if($start == ''){
            $start = 0;
        }
        if($limit == ''){
            $limit = maksimalRequest;
        }

        //untuk menambahkan custom query
        if($KODE != ''){
            $sqladd .= " having b.KODE = " . $this->db->escape($KODE) . " ";
        }
        $sql = "select b.KODE, NAMA, ALAMAT, count(*) JUMLAH_ORDER, sum(JUMLAH) JUMLAH_BELI, sum(a.poin) TOTAL_POIN, 
                (select ifnull(sum(c.POIN),0) from tb_tukar_poin c where c.id_toko = b.kode) TUKAR,
                sum(a.poin) - (select ifnull(sum(c.POIN),0) from tb_tukar_poin c where c.id_toko = b.kode) POIN
                    from  tb_poin_toko a, (select t.KODE, t.NAMA, t.ALAMAT, t.KOTA, sales.NAMA as NAMA_SALES, sales.NIK
                                                        FROM tb_toko t, (SELECT p.*
                                                                        FROM tb_pegawai p, (select * 
                                                                                            from tb_pegawai tb_p
                                                                                            where tb_p.NIK='$id_user') kadepo
                                                                        WHERE kadepo.unit = p.unit
                                                                        AND p.JOBDESK='SALES') sales
                                                        where t.KDSALES=sales.NIK) b
                    where b.KODE = a.ID_TOKO
                 group by a.ID_TOKO 
                " . $sqladd . "
                order by POIN desc
                 limit $start , $limit";

        $data = $this->db->query($sql);
        if($data->num_rows()>=0){
            return $data->result();
        }else{
            return false;
        }
    }
    
    public function getDetailPoin($KODE){
        $sql = "select a.NAMA, a.ALAMAT, b.NO_FAKTUR, date_format(b.TGL_FAKTUR, '%d-%m-%Y') TGL_FAKTUR, 
                 sum(c.JUMLAH) JUMLAH,  POIN
                from tb_toko a, tb_order b, tb_order_detail c, 
                      tb_produk d, tb_poin_toko e
                where a.KODE = b.KODE_TOKO and b.ID_ORDER = c.ID_ORDER  and is_approval = 1
                and c.ID_PRODUK = d.ID_PRODUK and b.ID_ORDER = e.ID_ORDER
                and a.KODE = " . $this->db->escape($KODE) . "
                group by a.KODE, b.NO_FAKTUR
                order by a.KODE,  date_format(b.TGL_FAKTUR, '%Y-%m-%d') desc
                limit 0,30";

        $data = $this->db->query($sql);
        if($data->num_rows()>=0){
            return $data->result();
        }else{
            return false;
        }
    }
    
    public function getDetailTukar($KODE){
        $sql = "select a.NAMA, a.ALAMAT, date_format(b.TGL_TUKAR, '%d-%m-%Y') TGL_TUKAR, POIN, REWARD
                from tb_toko a, tb_tukar_poin b
                where a.KODE = b.ID_TOKO
                and a.KODE = " . $this->db->escape($KODE) . "
                group by a.KODE, b.TGL_TUKAR
                order by a.KODE,  date_format(b.TGL_TUKAR, '%Y-%m-%d') desc
                limit 0,30";

        $data = $this->db->query($sql);
        if($data->num_rows()>=0){
            return $data->result();
        }else{
            return false;
        }
    }
    
    public function getPembayaran($id_user, $KODE, $TGL_AWAL, $TGL_AKHIR, $start, $limit){
        $sqladd     = "";
        
        if($start == ''){
            $start = 0;
        }
        if($limit == ''){
            $limit = maksimalRequest;
        }

        //untuk menambahkan custom query
        if($KODE != ''){
            $sqladd .= " and PELANGGAN = " . $this->db->escape($KODE) . " ";
        }
        if (strlen($TGL_AWAL) > 0 && strlen($TGL_AKHIR) > 0) {
            $sqladd .= " and TGL_KUITANSI BETWEEN " . $this->db->escape($TGL_AWAL) . " and " . $this->db->escape($TGL_AKHIR) . "";
        }

        $sql = "SELECT NAMA_PELANGGAN, ALAMAT_PELANGGAN, PEMBAYARAN, NO_KUITANSI, date_format(TGL_KUITANSI, '%d-%m-%Y') TGL_KUITANSI, 
                        NO_FAKTUR, date_format(TGL_FAKTUR, '%d-%m-%Y') TGL_FAKTUR, TOTAL TOTAL
                  FROM tb_pembayaran_toko a, (select t.KODE, t.NAMA, t.ALAMAT, t.KOTA, sales.NAMA as NAMA_SALES, sales.NIK
                                                        FROM tb_toko t, (SELECT p.*
                                                                        FROM tb_pegawai p, (select * 
                                                                                            from tb_pegawai tb_p
                                                                                            where tb_p.NIK='$id_user') kadepo
                                                                        WHERE kadepo.unit = p.unit
                                                                        AND p.JOBDESK='SALES') sales
                                                        where t.KDSALES=sales.NIK) b
                where a.PELANGGAN = b.KODE
                " . $sqladd . "
                 order by TGL_KUITANSI desc, NAMA_PELANGGAN
                 limit $start , $limit";

        $data = $this->db->query($sql);
        if($data->num_rows()>=0){
            return $data->result();
        }else{
            return false;
        }
    }
    // ---------------------------------------------dari Mas Pii-----------------------------------------------
    public function totalNotifHariIni($tanggal_id){
        $sql    ="select ifnull(count(*), 0) as total from tb_notif_pembayaran where ID_NOTIF like '$tanggal_id%'";

        $result = $this->db->query($sql)->result();

        return $result[0]->total;
    }
    public function simpanNotifPembayaran($data_notif){
        $this->db->insert('tb_notif_pembayaran', $data_notif);
    }
    // ---------------------------------------------untuk notifikasi kolektor-----------------------------------------------
    public function getDaftarPelunasan($id_user, $kode){       //fungsi ini juga ada di Apitokomodel
        $sqladd     = "";
        
//        $sql = "select date_format(y.TGL_NOTIF, '%d-%m-%Y') TGL_NOTIF, z.NAMA KOLEKTOR, x.NAMA, x.NO_FAKTUR, x.TGL_FAKTUR, PIUTANG PIUTANG, TGL_JATUH_TEMPO, y.TOTAL PEMBAYARAN,  date_format(y.TGL_PEMBAYARAN, '%d-%m-%Y') TGL_PEMBAYARAN,  
//                y.ID_NOTIF, x.KODE from (
//                select b.KODE, b.NAMA, b.ALAMAT, a.NO_FAKTUR, date_format(a.TGL_FAKTUR, '%d-%m-%Y') TGL_FAKTUR, a.TOTAL, (a.total - sum(x.TOTAL)) PIUTANG,  
//                        date_format(a.TGL_JATUH_TEMPO, '%d-%m-%Y') TGL_JATUH_TEMPO, date_format(a.TGL_JATUH_TEMPO, '%Y-%m-%d') TGL
//                        from tb_order a left outer join tb_pembayaran_toko x on a.NO_FAKTUR = x.NO_FAKTUR, tb_toko b
//                        where a.KODE_TOKO = b.KODE  and is_approval = 1
//                        and a.TOTAL > (select sum(z.TOTAL) from tb_pembayaran_toko z where a.NO_FAKTUR = z.NO_FAKTUR)
//                 union all
//                 select b.KODE, b.NAMA, b.ALAMAT, a.NO_FAKTUR, date_format(a.TGL_FAKTUR, '%d-%m-%Y') TGL_FAKTUR, a.TOTAL, a.total PIUTANG,  
//                        date_format(a.TGL_JATUH_TEMPO, '%d-%m-%Y') TGL_JATUH_TEMPO, date_format(a.TGL_JATUH_TEMPO, '%Y-%m-%d') TGL
//                        from tb_order a, tb_toko b
//                        where a.KODE_TOKO = b.KODE and is_approval = 1
//                        and no_faktur not in (select x.no_faktur from tb_pembayaran_toko x where a.NO_FAKTUR = x.NO_FAKTUR)
//                 ) x, tb_notif_pembayaran y, tb_pegawai z 
//                     where x.KODE = y.KODE_TOKO and y.KOLEKTOR = z.NIK
//                     and y.is_confirm = 1 and y.kolektor = '$id_user'
//                     and x.KODE='$kode'
//                     order by date_format(y.TGL_PEMBAYARAN, '%Y-%m-%d')
//                limit 0 , 1";
        
        $sql = "select date_format(y.TGL_NOTIF, '%d-%m-%Y') TGL_NOTIF, z.NAMA KOLEKTOR, x.NAMA, x.NO_FAKTUR, x.TGL_FAKTUR, PIUTANG PIUTANG, TGL_JATUH_TEMPO, y.TOTAL PEMBAYARAN,  date_format(y.TGL_PEMBAYARAN, '%d-%m-%Y') TGL_PEMBAYARAN,  
                y.ID_NOTIF, x.KODE from (
                select b.KODE, b.NAMA, b.ALAMAT, a.FAKTUR NO_FAKTUR, date_format(a.TANGGAL, '%d-%m-%Y') TGL_FAKTUR, a.JUMLAH TOTAL, (a.jumlah - sum(x.TOTAL)) PIUTANG,  
                        date_format(a.TGL_JATUH_TEMPO, '%d-%m-%Y') TGL_JATUH_TEMPO, date_format(a.TGL_JATUH_TEMPO, '%Y-%m-%d') TGL
                        from tb_penjualan a left outer join tb_pembayaran_toko x on a.FAKTUR = x.NO_FAKTUR, tb_toko b
                        where a.K_PLG = b.KODE
                        and a.JUMLAH > (select sum(z.TOTAL) from tb_pembayaran_toko z where a.FAKTUR = z.NO_FAKTUR)
                 union all
                 select b.KODE, b.NAMA, b.ALAMAT, a.FAKTUR NO_FAKTUR, date_format(a.TANGGAL, '%d-%m-%Y') TGL_FAKTUR, a.JUMLAH TOTAL, a.jumlah PIUTANG,  
                        date_format(a.TGL_JATUH_TEMPO, '%d-%m-%Y') TGL_JATUH_TEMPO, date_format(a.TGL_JATUH_TEMPO, '%Y-%m-%d') TGL
                        from tb_penjualan a, tb_toko b
                        where a.K_PLG = b.KODE
                        and faktur not in (select x.no_faktur from tb_pembayaran_toko x where a.FAKTUR = x.NO_FAKTUR)
                 ) x, tb_notif_pembayaran y, tb_pegawai z 
                     where x.KODE = y.KODE_TOKO and y.KOLEKTOR = z.NIK
                     and y.is_confirm = 1 and y.kolektor = '$id_user'
                     and x.KODE='$kode'
                     order by date_format(y.TGL_PEMBAYARAN, '%Y-%m-%d')
                limit 0 , 1";
                
        $data = $this->db->query($sql);
        if($data->num_rows()==1){
            $data = $data->result();
            return $data[0];
        }else{
            return false;
        }
    }
    public function getKoletor($KODE){
        $stat = "select k.ID_USER, k.USER_TOKEN, t.TELEPON from tb_user k, tb_toko t WHERE k.ID_USER=t.KDKOLEKTOR and t.KODE='$KODE'";
        $data = $this->db->query($stat);
        if($data->num_rows()==1){
            $data = $data->result();
            return $data[0];
        }else{
            return array();
        }
    }
}