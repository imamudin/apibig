<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Apikadepo extends MY_Controller {

    //method checkApp terletak di MY_Controller

    function __construct() {
        parent::__construct();
        $this->load->model('Apikadepomodel');
    }
    public function login()
    {
        //untuk mengecek apakah input dari aplikasi android
        if($this->checkApp($this->input->get_request_header('appToken'))){
            $status     = -1;
            $message    = '';
            $data       = '';

            $data = (object)json_decode(file_get_contents('php://input'));
            
            isset($data->user_name)     ? $username     = $data->user_name      : $username     = '';
            isset($data->user_password) ? $userpassword = $data->user_password  : $userpassword = '';

            $userlogin = $this->Apikadepomodel->login($username, $userpassword);

            if($userlogin != false){
                $date = date('YmdHis');
                $token = md5($date);

                //megnupdate token user
                if($this->Apikadepomodel->updateToken($username, $userpassword, $token)){
                    $id_user    = $userlogin[0]->id_user;
                    $data_kadepo= $this->Apikadepomodel->getKadepo($id_user);
                    
                    $status     = 1;
                    $message    = "Login success.";
                    $data       = array(
                            'kadepo'    => $data_kadepo
                        );
                }else{
                    $status     = 0;
                    $message    = "Tidak dapat melakukan input data.";
                    $data       = null;
                }
            }else{
                $message    = 'Username dan password tidak ditemukan.';
                $data       = null;
                $status     = 0;
            }
            $response   = array(
                    'status'    => $status,
                    'message'   => $message,
                    'data'      => $data
                );
            $this->makeOutput($response);
        }else{
            $this->jsonNoRespon();
        }
    }
    public function logout(){
        //untuk mengecek apakah input dari aplikasi android
        if($this->checkApp($this->input->get_request_header('appToken'))){
            $status     = -1;
            $message    = '';
            $data       = '';

            $data = (object)json_decode(file_get_contents('php://input'));
            
            isset($data->id_user)       ? $id_user      = $data->id_user        : $id_user      = '';
            isset($data->user_token)    ? $user_token   = $data->user_token     : $user_token   = '';

            $userlogout     = $this->Apikadepomodel->logout($id_user, $user_token);

            if($userlogout){
                $message    = 'Logout berhasil.';
                $data       = 'null';
                $status     = 1;
            }else{
                $message    = 'Logout gagal.';
                $data       = null;
                $status     = -1;
            }

            $response   = array(
                    'status'    => $status,
                    'message'     => $message,
                    'data'      => $data
                );
            $this->makeOutput($response);
        }else{
            $this->jsonNoRespon();
        }
    }
    public function getToko() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->START) ? $START = $data->START : $START = '';
            isset($data->LIMIT) ? $LIMIT = $data->LIMIT : $LIMIT = '';

            if ($id_user != '' && $user_token != '') {
                $data_toko = $this->Apikadepomodel->getToko($id_user, $START, $LIMIT);
                if ($data_toko) {
                    $order = array(
                        "total" => count($data_toko),
                        "toko" => $data_toko
                    );
                    $message = 'Data toko.';
                    $data = $order;
                    $status = 1;
                } else {
                    $message = 'Data toko tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }
    public function transaksi() {
        //untuk mengecek apakah input dari aplikasi android
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->KODE) ? $KODE = $data->KODE : $KODE = '';

            if ($id_user != '' && $user_token != '' && $KODE != '') {
                //mengecek waktu scan, harus diantara jam 08.00 - 15.30
                $jam    = date('H');
                $menit  = date('i');
                // if(($jam >= 8 && $jam <= 17)){
                //     $message = 'Maaf, transaksi pelanggan melebihi jam yang ditentukan (17:30 s/d 08:00)';
                //     $data = null;
                //     $status = 0;
                // }else{
                //     if($jam == 17 && ($menit <= 30)){
                //         //tidak bisa scan karena lebih dari jam setengah 4
                //         $message = 'Maaf, transaksi pelanggan melebihi jam yang ditentukan (17:30 s/d 08:00)';
                //         $data = null;
                //         $status = 0;  
                //     }else{
                        //mengecek login user
                        $is_login = $this->Apikadepomodel->getUserByToken($id_user, $user_token);
                        if ($is_login != false) {
                            //mengecek apakah ID Sales sesuai dengan toko atau tidak
                            $toko = $this->Apikadepomodel->getTokobyKode($KODE);
                            $long = 0;
                            $lati = 0;

                            if (count($toko) == 1) {
                                $is_toko    = $this->Apikadepomodel->checkTokoByKadepo($id_user, $KODE);
                                if($is_toko != false){
                                    $datetime   = date('Y-m-d H:i:s');
                                    $tanggal    = substr($datetime, 0, 10);
                                    $jam_mulai  = substr($datetime, 11);
                                    $poinToko   = $this->Apikadepomodel->getPoin($KODE);
                                    $piutangToko= $this->Apikadepomodel->getPiutang($KODE);

                                    $newdata = array(
                                        'poin' => $poinToko,
                                        'piutang' => $piutangToko,
                                    );

                                    $message= "Scan berhasil.";
                                    $data   = $newdata;
                                    $status = 1;

                                }else{
                                    $message = 'Maaf, barcode yang anda scan bukan pelanggan anda.';
                                    $data = null;
                                    $status = 0;
                                }
                            }else{
                                $message = 'Data Toko tidak ditemukan.';
                                $data = null;
                                $status = 0;
                            }
                        } else {
                            $message = 'Akun tidak ditemukan.';
                            $data = null;
                            $status = 0;
                        }
                //     }
                // }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }

    
    public function cekFaktur() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->KODE) ? $KODE = $data->KODE : $KODE = '';

            if ($id_user != '' && $user_token != '' && $KODE != '') {
                //mengecek login user
                $is_login = $this->Apikadepomodel->getUserByToken($id_user, $user_token);
                if ($is_login) {
                    //mengecek faktur dari toko, apabila ada return true, dapat melakukan order, jika tidak, tidak dapat
                    $faktur = $this->Apikadepomodel->cekFaktur($KODE);

                    $harga = $this->Apikadepomodel->getHarga();
                    $gudang = $this->Apikadepomodel->getGudang();
                    $total_piutang = $this->Apikadepomodel->getTotalPiutang($KODE);
                    $default_gudang = $this->Apikadepomodel->getDefaultGudang($id_user);

                    $status = 1;
                    $message = 'Data form order.';
                    $newdata = array(
                        'harga' => $harga,
                        'gudang' => $gudang,
                        'total_piutang' => $total_piutang,
                        'default_gudang' => $default_gudang,
                        'ada_faktur' => $faktur
                    );

                    $data = $newdata;
                } else {
                    $message = 'Akun tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }
    public function simpanOrder() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->jumlah_pesanan) ? $jumlah_pesanan = $data->jumlah_pesanan : $jumlah_pesanan = array();
            isset($data->HARGA) ? $HARGA = $data->HARGA : $HARGA = array();
            isset($data->ID_PRODUK) ? $ID_PRODUK = $data->ID_PRODUK : $ID_PRODUK = array();
            isset($data->tanggal_kirim) ? $tanggal_kirim = $data->tanggal_kirim : $tanggal_kirim = '';
            isset($data->JENIS_PENGIRIMAN) ? $JENIS_PENGIRIMAN = $data->JENIS_PENGIRIMAN : $JENIS_PENGIRIMAN = '';
            isset($data->KODE) ? $KODE = $data->KODE : $KODE = '';       //untuk kode toko
            isset($data->total_harga) ? $total_harga = $data->total_harga : $total_harga = '';
            isset($data->KODE_GUDANG) ? $KODE_GUDANG = $data->KODE_GUDANG : $KODE_GUDANG = '';
            isset($data->is_approval) ? $is_approval = $data->is_approval : $is_approval = 1;

            if ($id_user != '' && $user_token != '' && $tanggal_kirim != '' && $jumlah_pesanan != '' && $KODE_GUDANG != '' && $KODE != '' && $total_harga != '' && count($HARGA) > 0 && count($jumlah_pesanan) > 0 && count($ID_PRODUK) > 0 && $JENIS_PENGIRIMAN != '') {
                //mengecek login user
                $is_login = $this->Apikadepomodel->getUserByToken($id_user, $user_token);
                if ($is_login) {
                    //pertama insert ke tb order
                    $tanggal = date('Y-m-d');
                    $tanggal_id = date('ymd');
                    $order_ke = ($this->Apikadepomodel->totalOrderHariIni($tanggal_id) + 1);

                    //membuat faktur
                    // $tahun_bulan = date("Ym");
                    // $bulan = date('m');

                    // if ($is_approval == 1) {
                    //     if ($JENIS_PENGIRIMAN == CD) {
                    //         $bulan_romawi = unserialize(bulan_romawi);

                    //         $bulan_romawi = $bulan_romawi['' . $bulan];

                    //         $setengah_faktur = $tahun_bulan . '/' . $bulan_romawi . '/' . $KODE_GUDANG;

                    //         $faktur_ke = ($this->Apikadepomodel->getTotalFakturBulanIni($setengah_faktur) + 1);
                    //         ;

                    //         if ($faktur_ke < 10) {
                    //             $faktur = $setengah_faktur . '000' . $faktur_ke;
                    //         } else if ($faktur_ke >= 10 && $faktur_ke < 100) {
                    //             $faktur = $setengah_faktur . '00' . $faktur_ke;
                    //         } else if ($faktur_ke >= 100 && $faktur_ke < 1000) {
                    //             $faktur = $setengah_faktur . '0' . $faktur_ke;
                    //         } else {
                    //             $faktur = $setengah_faktur . '' . $faktur_ke;
                    //         }
                    //     } else if ($JENIS_PENGIRIMAN == TRONTON) {
                    //         $setengah_faktur = $tahun_bulan . $KODE_GUDANG;

                    //         $faktur_ke = ($this->Apikadepomodel->getTotalFakturBulanIni($setengah_faktur) + 1);
                    //         ;

                    //         if ($faktur_ke < 10) {
                    //             $faktur = $setengah_faktur . '000' . $faktur_ke;
                    //         } else if ($faktur_ke >= 10 && $faktur_ke < 100) {
                    //             $faktur = $setengah_faktur . '00' . $faktur_ke;
                    //         } else if ($faktur_ke >= 100 && $faktur_ke < 1000) {
                    //             $faktur = $setengah_faktur . '0' . $faktur_ke;
                    //         } else {
                    //             $faktur = $setengah_faktur . '' . $faktur_ke;
                    //         }
                    //     }
                    // } else {
                    //     $faktur = "";
                    // }

                    $faktur = "-";
                    //tutup membuat faktur

                    $tgl_faktur = date('Y-m-d');
                    $jatuh_tempo_toko = $this->Apikadepomodel->getJatuhTempo($KODE);
                    $nik_sales = $this->Apikadepomodel->getNIKSales($KODE);
                    $tgl_jth_tempo = date('Y-m-d', strtotime("+$jatuh_tempo_toko days"));

                    if ($order_ke < 10) {
                        $id_order = $tanggal_id . '000' . $order_ke;
                    } else if ($order_ke >= 10 && $order_ke < 100) {
                        $id_order = $tanggal_id . '00' . $order_ke;
                    } else if ($order_ke >= 100 && $order_ke < 1000) {
                        $id_order = $tanggal_id . '0' . $order_ke;
                    } else {
                        $id_order = $tanggal_id . '' . $order_ke;
                    }

                    $insert_order = $this->Apikadepomodel->insertorder($id_order, $nik_sales, $KODE, $faktur, $tgl_faktur, $total_harga, $KODE_GUDANG, $tgl_jth_tempo, $tanggal_kirim, $JENIS_PENGIRIMAN, $is_approval);

                    if ($insert_order) {
                        //insert detail order
                        $total_pesanan = 0;
                        for ($i = 0; $i < count($ID_PRODUK); $i++) {
                            $this->Apikadepomodel->insertDetailOrder($id_order, $ID_PRODUK[$i], $jumlah_pesanan[$i], $HARGA[$i], $jumlah_pesanan[$i] * $HARGA[$i]);

                            $total_pesanan += $jumlah_pesanan[$i];
                        }

                        //menghitung poin
                        $bagi_poin = $this->Apikadepomodel->getPoinPerPembelian();
                        $total_poin = ($total_pesanan) / $bagi_poin;
                        $s_poin = "" . $total_poin;
                        $poins = explode('.', $s_poin);
                        $poin = 0;
                        if (count($poins) >= 0) {
                            $poin = $poins[0];
                        }
                        //menyimpan stok
                        for ($i = 0; $i < count($ID_PRODUK); $i++) {
                            $insert_stok1 = $this->Apikadepomodel->insertStok($KODE, $ID_PRODUK[$i], $tanggal, 1, $id_order, $jumlah_pesanan[$i]);
                        }
                        //menyimpan poin
                        $insert_poin = $this->Apikadepomodel->insertPoin($KODE, $id_order, ($jumlah_pesanan[0] + $jumlah_pesanan[0]), $poin);

                        $message = "Data berhasil disimpan.";
                        $data = null;
                        $status = 1;
                    } else {
                        $message = "Gagal menyimpan data.";
                        $data = null;
                        $status = 0;
                    }
                } else {
                    $message = 'Akun tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }
    public function cekFakturNonSemen() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->KODE) ? $KODE = $data->KODE : $KODE = '';

            if ($id_user != '' && $user_token != '' && $KODE != '') {
                //mengecek login user
                $is_login = $this->Apikadepomodel->getUserByToken($id_user, $user_token);
                if ($is_login) {
                    //mengecek faktur dari toko, apabila ada return true, dapat melakukan order, jika tidak, tidak dapat
                    $faktur = $this->Apikadepomodel->cekFaktur($KODE);

                    $produknonsemen   = $this->Apikadepomodel->getProdukNonSemen();
                    $gudang             = $this->Apikadepomodel->getGudang();
                    $total_piutang      = $this->Apikadepomodel->getTotalPiutang($KODE);
                    $default_gudang     = $this->Apikadepomodel->getDefaultGudang($id_user);

                    $status = 1;
                    $message = 'Data form order.';
                    $newdata = array(
                        'produknonsemen' => $produknonsemen,
                        'gudang' => $gudang,
                        'total_piutang' => $total_piutang,
                        'default_gudang' => $default_gudang,
                        'ada_faktur' => $faktur
                    );

                    $data = $newdata;
                } else {
                    $message = 'Akun tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }

    
    public function simpanOrderNonSemen() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->jumlah_pesanan) ? $jumlah_pesanan = $data->jumlah_pesanan : $jumlah_pesanan = array();
            isset($data->HARGA) ? $HARGA = $data->HARGA : $HARGA = array();
            isset($data->ID_PRODUK) ? $ID_PRODUK = $data->ID_PRODUK : $ID_PRODUK = array();
            isset($data->tanggal_kirim) ? $tanggal_kirim = $data->tanggal_kirim : $tanggal_kirim = '';
            isset($data->KODE) ? $KODE = $data->KODE : $KODE = '';       //untuk kode toko
            isset($data->total_harga) ? $total_harga = $data->total_harga : $total_harga = '';
            isset($data->KODE_GUDANG) ? $KODE_GUDANG = $data->KODE_GUDANG : $KODE_GUDANG = '';
            isset($data->is_approval) ? $is_approval = $data->is_approval : $is_approval = 1;

            if ($id_user != '' && $user_token != '' && $tanggal_kirim != '' 
            && $jumlah_pesanan != '' && $KODE_GUDANG != '' 
            && $KODE != '' && $total_harga != '' && count($HARGA) > 0 
            && count($jumlah_pesanan) > 0 && count($ID_PRODUK) > 0 ) {
                //mengecek login user
                $is_login = $this->Apikadepomodel->getUserByToken($id_user, $user_token);
                if ($is_login) {
                    //pertama insert ke tb order
                    $tanggal = date('Y-m-d');
                    $tanggal_id = date('ymd');
                    $order_ke = ($this->Apikadepomodel->totalOrderHariIniNonSemen($tanggal_id) + 1);

                    $faktur = "-";
                    //tutup membuat faktur

                    $tgl_faktur = date('Y-m-d');
                    $jatuh_tempo_toko = $this->Apikadepomodel->getJatuhTempo($KODE);
                    $nik_sales = $this->Apikadepomodel->getNIKSales($KODE);
                    $tgl_jth_tempo = date('Y-m-d', strtotime("+$jatuh_tempo_toko days"));

                    if ($order_ke < 10) {
                        $id_order = $tanggal_id . '000' . $order_ke;
                    } else if ($order_ke >= 10 && $order_ke < 100) {
                        $id_order = $tanggal_id . '00' . $order_ke;
                    } else if ($order_ke >= 100 && $order_ke < 1000) {
                        $id_order = $tanggal_id . '0' . $order_ke;
                    } else {
                        $id_order = $tanggal_id . '' . $order_ke;
                    }

                    $insert_order = $this->Apikadepomodel->insertorderNonSemen($id_order, $nik_sales, $KODE, 
                    $faktur, $tgl_faktur, $total_harga, $KODE_GUDANG, $tgl_jth_tempo, $tanggal_kirim, 
                    "-", $is_approval);

                    if ($insert_order) {
                        //insert detail order
                        $total_pesanan = 0;
                        for ($i = 0; $i < count($ID_PRODUK); $i++) {
                            $this->Apikadepomodel->insertDetailOrderNonSemen($id_order, $ID_PRODUK[$i], $jumlah_pesanan[$i], $HARGA[$i], $jumlah_pesanan[$i] * $HARGA[$i]);

                            $total_pesanan += $jumlah_pesanan[$i];
                        }

                        $message = "Data berhasil disimpan.";
                        $data = null;
                        $status = 1;
                    } else {
                        $message = "Gagal menyimpan data.";
                        $data = null;
                        $status = 0;
                    }
                } else {
                    $message = 'Akun tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }


    public function getOrder() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->KODE) ? $KODE = $data->KODE : $KODE = '';
            isset($data->TGL_AWAL) ? $TGL_AWAL = $data->TGL_AWAL : $TGL_AWAL = '';
            isset($data->TGL_AKHIR) ? $TGL_AKHIR = $data->TGL_AKHIR : $TGL_AKHIR = '';
            isset($data->START) ? $START = $data->START : $START = '';
            isset($data->LIMIT) ? $LIMIT = $data->LIMIT : $LIMIT = '';


            if ($id_user != '' && $user_token != '' && $TGL_AWAL != '' && $TGL_AKHIR != '') {
                //mengecek login user
                $is_login = $this->Apikadepomodel->getUserByToken($id_user, $user_token);
                if ($is_login) {
                    $data_kunjungan = $this->Apikadepomodel->getOrders($id_user, $KODE, $TGL_AWAL, $TGL_AKHIR, $START, $LIMIT);
                    // var_dump($data_kunjungan);
                    // exit();
                    if ($data_kunjungan) {
                        $kunjungan = array(
                            "total" => count($data_kunjungan),
                            "order" => $data_kunjungan
                        );
                        $message = 'Data order.';
                        $data = $kunjungan;
                        $status = 1;
                    } else {
                        $message = 'Data order tidak ditemukan.';
                        $data = null;
                        $status = 0;
                    }
                } else {
                    $message = 'Akun tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }
    public function getOrderNonSemen() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->KODE) ? $KODE = $data->KODE : $KODE = '';
            isset($data->TGL_AWAL) ? $TGL_AWAL = $data->TGL_AWAL : $TGL_AWAL = '';
            isset($data->TGL_AKHIR) ? $TGL_AKHIR = $data->TGL_AKHIR : $TGL_AKHIR = '';
            isset($data->START) ? $START = $data->START : $START = '';
            isset($data->LIMIT) ? $LIMIT = $data->LIMIT : $LIMIT = '';


            if ($id_user != '' && $user_token != '' && $TGL_AWAL != '' && $TGL_AKHIR != '') {
                //mengecek login user
                $is_login = $this->Apikadepomodel->getUserByToken($id_user, $user_token);
                if ($is_login) {
                    $data_kunjungan = $this->Apikadepomodel->getOrdersNonSemen($id_user, $KODE, $TGL_AWAL, $TGL_AKHIR, $START, $LIMIT);
                    // var_dump($data_kunjungan);
                    // exit();
                    if ($data_kunjungan) {
                        $kunjungan = array(
                            "total" => count($data_kunjungan),
                            "order" => $data_kunjungan
                        );
                        $message = 'Data order.';
                        $data = $kunjungan;
                        $status = 1;
                    } else {
                        $message = 'Data order tidak ditemukan.';
                        $data = null;
                        $status = 0;
                    }
                } else {
                    $message = 'Akun tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }

    public function getDetailOrder() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->ID_ORDER) ? $ID_ORDER = $data->ID_ORDER : $ID_ORDER = '';

            if ($id_user != '' && $user_token != '' && $ID_ORDER != '') {
                //mengecek login user
                $is_login = $this->Apikadepomodel->getUserByToken($id_user, $user_token);
                if ($is_login) {
                    $data_order = $this->Apikadepomodel->getDetailOrders($ID_ORDER);
                    if ($data_order) {
                        $order = array(
                            "total" => count($data_order),
                            "order" => $data_order
                        );
                        $message = 'Data detail order.';
                        $data = $order;
                        $status = 1;
                    } else {
                        $message = 'Data detail order tidak ditemukan.';
                        $data = null;
                        $status = 0;
                    }
                } else {
                    $message = 'Akun tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }
    public function getDetailOrderNonSemen() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->ID_ORDER) ? $ID_ORDER = $data->ID_ORDER : $ID_ORDER = '';

            if ($id_user != '' && $user_token != '' && $ID_ORDER != '') {
                //mengecek login user
                $is_login = $this->Apikadepomodel->getUserByToken($id_user, $user_token);
                if ($is_login) {
                    $data_order = $this->Apikadepomodel->getDetailOrdersNonSemen($ID_ORDER);
                    if ($data_order) {
                        $order = array(
                            "total" => count($data_order),
                            "order" => $data_order
                        );
                        $message = 'Data detail order.';
                        $data = $order;
                        $status = 1;
                    } else {
                        $message = 'Data detail order tidak ditemukan.';
                        $data = null;
                        $status = 0;
                    }
                } else {
                    $message = 'Akun tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }

    public function getPiutang() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->KODE) ? $KODE = $data->KODE : $KODE = '';
            isset($data->START) ? $START = $data->START : $START = '';
            isset($data->LIMIT) ? $LIMIT = $data->LIMIT : $LIMIT = '';

            if ($id_user != '' && $user_token != '') {
                //mengecek login user
                $is_login = $this->Apikadepomodel->getUserByToken($id_user, $user_token);
                if ($is_login) {
                    $data_piutang = $this->Apikadepomodel->getPiutangs($id_user, $KODE, $START, $LIMIT);
                    // var_dump($data_kunjungan);
                    // exit();
                    if ($data_piutang) {
                        $kunjungan = array(
                            "total" => count($data_piutang),
                            "piutang" => $data_piutang
                        );
                        $message = 'Data piutang.';
                        $data = $kunjungan;
                        $status = 1;
                    } else {
                        $message = 'Data piutang tidak ditemukan.';
                        $data = null;
                        $status = 0;
                    }
                } else {
                    $message = 'Akun tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }

    public function getDetailPiutang() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->KODE) ? $KODE = $data->KODE : $KODE = '';

            if ($id_user != '' && $user_token != '' && $KODE != '') {
                //mengecek login user
                $is_login = $this->Apikadepomodel->getUserByToken($id_user, $user_token);
                if ($is_login) {
                    $data_piutang = $this->Apikadepomodel->getDetailPiutang($KODE);
                    if ($data_piutang) {
                        $order = array(
                            "total" => count($data_piutang),
                            "piutang" => $data_piutang
                        );
                        $message = 'Data detail Piutang.';
                        $data = $order;
                        $status = 1;
                    } else {
                        $message = 'Data detail piutang tidak ditemukan.';
                        $data = null;
                        $status = 0;
                    }
                } else {
                    $message = 'Akun tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }

    public function getTarget() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            if ($id_user != '' && $user_token != '') {
                //mengecek login user
                $is_login = $this->Apikadepomodel->getUserByToken($id_user, $user_token);
                if ($is_login) {
                    $tahun_bulan = date("Ym");
                    $bulan = date("m");

                    $data_target = $this->Apikadepomodel->getTarget($id_user, $tahun_bulan, $bulan);

                    $bulan_string = unserialize(bulan_string);
                    $bulan_string = $bulan_string['' . $bulan];

                    $waktu = array(
                        "tahun" => date("Y"),
                        "bulan" => $bulan,
                        "bulan_string" => $bulan_string
                    );

                    $data_target = array(
                        "waktu" => $waktu,
                        "data" => $data_target
                    );


                    if ($data_target) {
                        $message = 'Data Target';
                        $data = $data_target;
                        $status = 1;
                    } else {
                        $message = 'Data Target order tidak ditemukan.';
                        $data = null;
                        $status = 0;
                    }
                } else {
                    $message = 'Akun tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'target' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }

    // --------------------------------------dari Mas Pii------------------------------------------
    public function getPoin() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->KODE) ? $KODE = $data->KODE : $KODE = '';
//            isset($data->TGL_AWAL)      ? $TGL_AWAL     = $data->TGL_AWAL       : $TGL_AWAL     = '';
//            isset($data->TGL_AKHIR)     ? $TGL_AKHIR    = $data->TGL_AKHIR      : $TGL_AKHIR    = '';
            isset($data->START) ? $START = $data->START : $START = '';
            isset($data->LIMIT) ? $LIMIT = $data->LIMIT : $LIMIT = '';


            if ($id_user != '' && $user_token != '') {
                //mengecek login user
                $is_login = $this->Apikadepomodel->getUserByToken($id_user, $user_token);
                if ($is_login) {
                    $data_poin = $this->Apikadepomodel->getPoinPelanggan($id_user, $KODE, $START, $LIMIT);
                    // var_dump($data_kunjungan);
                    // exit();
                    if ($data_poin) {
                        $poin = array(
                            "total" => count($data_poin),
                            "poin" => $data_poin
                        );
                        $message = 'Data Poin Pelanggan.';
                        $data = $poin;
                        $status = 1;
                    } else {
                        $message = 'Data poin pelanggan tidak ditemukan.';
                        $data = null;
                        $status = 0;
                    }
                } else {
                    $message = 'Akun tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }

    public function getDetailPoin() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->KODE) ? $KODE = $data->KODE : $KODE = '';

            if ($id_user != '' && $user_token != '' && $KODE != '') {
                //mengecek login user
                $is_login = $this->Apikadepomodel->getUserByToken($id_user, $user_token);
                if ($is_login) {
                    $data_poin = $this->Apikadepomodel->getDetailPoin($KODE);
                    if ($data_poin) {
                        $order = array(
                            "total" => count($data_poin),
                            "poin" => $data_poin
                        );
                        $message = 'Data detail poin pelanggan.';
                        $data = $order;
                        $status = 1;
                    } else {
                        $message = 'Data detail poin tidak ditemukan.';
                        $data = null;
                        $status = 0;
                    }
                } else {
                    $message = 'Akun tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }

    public function getDetailTukar() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->KODE) ? $KODE = $data->KODE : $KODE = '';

            if ($id_user != '' && $user_token != '' && $KODE != '') {
                //mengecek login user
                $is_login = $this->Apikadepomodel->getUserByToken($id_user, $user_token);
                if ($is_login) {
                    $data_tukar = $this->Apikadepomodel->getDetailTukar($KODE);
                    if ($data_tukar) {
                        $order = array(
                            "total" => count($data_tukar),
                            "tukar" => $data_tukar
                        );
                        $message = 'Data detail tukar poin pelanggan.';
                        $data = $order;
                        $status = 1;
                    } else {
                        $message = 'Data detail tukar poin tidak ditemukan.';
                        $data = null;
                        $status = 0;
                    }
                } else {
                    $message = 'Akun tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }

    public function getPembayaran() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->KODE) ? $KODE = $data->KODE : $KODE = '';
            isset($data->TGL_AWAL) ? $TGL_AWAL = $data->TGL_AWAL : $TGL_AWAL = '';
            isset($data->TGL_AKHIR) ? $TGL_AKHIR = $data->TGL_AKHIR : $TGL_AKHIR = '';
            isset($data->START) ? $START = $data->START : $START = '';
            isset($data->LIMIT) ? $LIMIT = $data->LIMIT : $LIMIT = '';


            if ($id_user != '' && $user_token != '' && $TGL_AWAL != '' && $TGL_AKHIR != '') {
                //mengecek login user
                $is_login = $this->Apikadepomodel->getUserByToken($id_user, $user_token);
                if ($is_login) {
                    $data_pembayaran = $this->Apikadepomodel->getPembayaran($id_user, $KODE, $TGL_AWAL, $TGL_AKHIR, $START, $LIMIT);
                    if ($data_pembayaran) {
                        $pembayaran = array(
                            "total" => count($data_pembayaran),
                            "pembayaran" => $data_pembayaran
                        );
                        $message = 'Data Pembayaran.';
                        $data = $pembayaran;
                        $status = 1;
                    } else {
                        $message = 'Data pembayaran tidak ditemukan.';
                        $data = null;
                        $status = 0;
                    }
                } else {
                    $message = 'Akun tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }

    // --------------------------------------dari Mas Pii------------------------------------------
    
    public function getDetailPelunasan(){
        if($this->checkApp($this->input->get_request_header('appToken'))){
            $status     = -1;
            $message    = '';
            $data       = '';

            $data = (object)json_decode(file_get_contents('php://input'));
            
            isset($data->id_user)       ? $id_user      = $data->id_user        : $id_user      = '';
            isset($data->user_token)    ? $user_token   = $data->user_token     : $user_token   = '';
            
            isset($data->KODE)          ? $KODE         = $data->KODE           : $KODE         = '';

            if($id_user != '' && $user_token != '' && $KODE != ''){
                //mengecek login user
                $is_login   = $this->Apikadepomodel->getUserByToken($id_user, $user_token);
                if($is_login){
                    $data_piutang = $this->Apikadepomodel->getDetailPiutang($KODE);
                    if($data_piutang){
                        $order  = array(
                                "total"     => count($data_piutang),
                                "piutang"   => $data_piutang
                            );
                        $message    = 'Data detail Piutang.';
                        $data       = $order;
                        $status     = 1;
                    }else{
                        $message    = 'Data detail piutang tidak ditemukan.';
                        $data       = null;
                        $status     = 0;
                    }
                }else{
                    $message    = 'Akun tidak ditemukan.';
                    $data       = null;
                    $status     = 0;
                }
            }else{
                $message    = 'Field is empty.';
                $data       = null;
                $status     = 0;
            }
            $response   = array(
                    'status'    => $status,
                    'message'   => $message,
                    'data'      => $data
                );
            $this->makeOutput($response);
        }else{
            $this->jsonNoRespon();
        }
    }
    public function bayarPelunasan(){
        if($this->checkApp($this->input->get_request_header('appToken'))){
            $status     = -1;
            $message    = '';
            $data       = '';

            $data = (object)json_decode(file_get_contents('php://input'));
            
            isset($data->id_user)       ? $id_user      = $data->id_user        : $id_user      = '';
            isset($data->user_token)    ? $user_token   = $data->user_token     : $user_token   = '';
            
            isset($data->KODE)          ? $KODE         = $data->KODE           : $KODE         = '';
            isset($data->jumlah_bayar)  ? $jumlah_bayar = $data->jumlah_bayar   : $jumlah_bayar = '';
            isset($data->tgl_bayar)     ? $tgl_bayar    = $data->tgl_bayar      : $tgl_bayar    = '';
            isset($data->faktur_bayar)  ? $faktur_bayar = $data->faktur_bayar   : $faktur_bayar = '';

            if($id_user != '' && $user_token != '' && $KODE != '' && $jumlah_bayar != '' && $tgl_bayar != '' && $faktur_bayar != ''){
                //mengecek login user
                $is_login   = $this->Apikadepomodel->getUserByToken($id_user, $user_token);
                if($is_login){
                    //untuk membuat id notif
                    $tanggal    = date('Y-m-d');
                    $tanggal_id = date('ymd');
                    $notif_ke   = ($this->Apikadepomodel->totalNotifHariIni($tanggal_id)+1);

                    if($notif_ke<10){
                        $id_notif   = $tanggal_id.'000'.$notif_ke;
                    }else if($notif_ke>=10 && $notif_ke<100){
                        $id_notif   = $tanggal_id.'00'.$notif_ke;
                    }else if($notif_ke>=100 && $notif_ke<1000){
                        $id_notif   = $tanggal_id.'0'.$notif_ke;
                    }else{
                        $id_notif   = $tanggal_id.''.$notif_ke;
                    }

                    $kolektor   = $this->Apikadepomodel->getKoletor($KODE);
                    $id_kolektor= $kolektor->ID_USER;

                    $data_notif = array(
                        'ID_NOTIF'      => $id_notif,
                        'TGL_NOTIF'     => $tanggal,
                        'NIK'           => $id_user,
                        'KODE_TOKO'     => $KODE,
                        'NO_FAKTUR'     => $faktur_bayar,
                        'TOTAL'         => $jumlah_bayar,
                        'TGL_PEMBAYARAN'=> $tgl_bayar,
                        'is_confirm'    => 1,
                        'KOLEKTOR'      => $id_kolektor,
                    );

                    //menyimpan notif ke database
                    $this->Apikadepomodel->simpanNotifPembayaran($data_notif);

                    //mengirim notifikasi ke kolektor
                    $message    = $this->Apikadepomodel->getDaftarPelunasan($id_kolektor, $KODE);

                    $msg = array(
                         'body' => $message,
                         'tipe' => 1,           //tipe 1 untuk notifikasi ketika ada pembayaran
                         'title' => "PUSH NOTIFICATION"
                    );

                    $registatoin_ids = array(
                        $kolektor->USER_TOKEN
                        );
                    if($message != null & $kolektor != null){
                        $this->sendNotificationKolektor($registatoin_ids, $msg);
                    }

                    
                    $message    = 'Permintaan pembayaran berhasil disimpan.';
                    $data       = null;
                    $status     = 1;
                }else{
                    $message    = 'Akun tidak ditemukan.';
                    $data       = null;
                    $status     = 0;
                }
            }else{
                $message    = 'Field is empty.';
                $data       = null;
                $status     = 0;
            }
            $response   = array(
                    'status'    => $status,
                    'message'   => $message,
                    'data'      => $data
                );
            $this->makeOutput($response);
        }else{
            $this->jsonNoRespon();
        }
    }
}

