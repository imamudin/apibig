<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Apikolektor extends MY_Controller {

    //method checkApp terletak di MY_Controller

    function __construct() {
        parent::__construct();
        $this->load->model('Apikolektormodel');
    }

    public function kirimNotifFCM() {
        $kode = $this->uri->segment(3);
        if ($kode != '') {
            $message = $this->Apikolektormodel->getTagihan($kode);

            $msg = array(
                'body' => $message,
                'tipe' => 1, //tipe 1 untuk notifikasi ketika ada pembayaran
                'title' => "PUSH NOTIFICATION"
            );

            $registatoin_ids = array(
                $message->USER_REGID
            );
            $this->sendNotification($registatoin_ids, $msg);
        }
    }

    public function login() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->user_name) ? $user_name = $data->user_name : $user_name = '';
            isset($data->user_password) ? $user_password = $data->user_password : $user_password = '';

            isset($data->user_regid) ? $user_regid = $data->user_regid : $user_regid = '';

            if ($user_name != '' && $user_password != '' && $user_regid != '') {
                $userlogin = $this->Apikolektormodel->login($user_name, $user_password);
                if ($userlogin) {
                    //megnupdate token user
                    if ($this->Apikolektormodel->updateToken($user_name, $user_password, $user_regid)) {
                        //mencetak data user
                        $data_user = $this->Apikolektormodel->getuser($user_name, $user_password);
                        $status = 1;
                        $message = "Login success.";
                        $data = $data_user[0];
                    } else {
                        $status = 0;
                        $message = "Tidak dapat melakukan input data.";
                        $data = null;
                    }
                } else {
                    $message = 'Username dan password tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field tidak boleh kosong.';
                $data = null;
                $status = 0;
            }

            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }

    public function logout() {
        //untuk mengecek apakah input dari aplikasi android
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            $userlogout = $this->Apikolektormodel->logout($id_user, $user_token);

            if ($userlogout) {
                $message = 'Logout berhasil.';
                $data = 'null';
                $status = 1;
            } else {
                $message = 'Logout gagal.';
                $data = null;
                $status = -1;
            }

            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }

    public function getPiutang() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->KODE) ? $KODE = $data->KODE : $KODE = '';
            isset($data->START) ? $START = $data->START : $START = '';
            isset($data->LIMIT) ? $LIMIT = $data->LIMIT : $LIMIT = '';

            if ($id_user != '' && $user_token != '') {
                //mengecek login user
                $is_login = $this->Apikolektormodel->getUserByToken($id_user, $user_token);
                if ($is_login) {
                    $data_piutang = $this->Apikolektormodel->getPiutangs($id_user, $KODE, $START, $LIMIT);
                    if ($data_piutang) {
                        $kunjungan = array(
                            "total" => count($data_piutang),
                            "piutang" => $data_piutang
                        );
                        $message = 'Data piutang.';
                        $data = $kunjungan;
                        $status = 1;
                    } else {
                        $message = 'Data piutang tidak ditemukan.';
                        $data = null;
                        $status = 0;
                    }
                } else {
                    $message = 'Akun tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }

    public function getDetailPiutang() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->KODE) ? $KODE = $data->KODE : $KODE = '';

            if ($id_user != '' && $user_token != '' && $KODE != '') {
                //mengecek login user
                $is_login = $this->Apikolektormodel->getUserByToken($id_user, $user_token);
                if ($is_login) {
                    $data_piutang = $this->Apikolektormodel->getDetailPiutang($KODE);
                    if ($data_piutang) {
                        $order = array(
                            "total" => count($data_piutang),
                            "piutang" => $data_piutang
                        );
                        $message = 'Data detail Piutang.';
                        $data = $order;
                        $status = 1;
                    } else {
                        $message = 'Data detail piutang tidak ditemukan.';
                        $data = null;
                        $status = 0;
                    }
                } else {
                    $message = 'Akun tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }

    public function getDaftarPelunasan() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->TGL_AWAL) ? $TGL_AWAL = $data->TGL_AWAL : $TGL_AWAL = '';
            isset($data->TGL_AKHIR) ? $TGL_AKHIR = $data->TGL_AKHIR : $TGL_AKHIR = '';
            isset($data->START) ? $START = $data->START : $START = '';
            isset($data->LIMIT) ? $LIMIT = $data->LIMIT : $LIMIT = '';


            if ($id_user != '' && $user_token != '') {
                //mengecek login user
                $is_login = $this->Apikolektormodel->getUserByToken($id_user, $user_token);
                if ($is_login) {
                    $data_pelunasan = $this->Apikolektormodel->getDaftarPelunasan($id_user, $TGL_AWAL, $TGL_AKHIR, $START, $LIMIT);
                    // var_dump($data_kunjungan);
                    // exit();
                    if ($data_pelunasan) {
                        $pelunasan = array(
                            "total" => count($data_pelunasan),
                            "pelunasan" => $data_pelunasan
                        );
                        $message = 'Data Pembayaran.';
                        $data = $pelunasan;
                        $status = 1;
                    } else {
                        $message = 'Data Pembayaran tidak ditemukan.';
                        $data = null;
                        $status = 0;
                    }
                } else {
                    $message = 'Akun tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }

    public function getToko() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->START) ? $START = $data->START : $START = '';
            isset($data->LIMIT) ? $LIMIT = $data->LIMIT : $LIMIT = '';

            if ($id_user != '' && $user_token != '') {
                $data_toko = $this->Apikolektormodel->getToko($id_user, $START, $LIMIT);
                if ($data_toko) {
                    $order = array(
                        "total" => count($data_toko),
                        "toko" => $data_toko
                    );
                    $message = 'Data toko.';
                    $data = $order;
                    $status = 1;
                } else {
                    $message = 'Data toko tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }

    public function pembayaranTagihan() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->NO_FAKTUR) ? $NO_FAKTUR = $data->NO_FAKTUR : $NO_FAKTUR = '';
            isset($data->KODE) ? $KODE = $data->KODE : $KODE = '';
            isset($data->TOTAL) ? $TOTAL = $data->TOTAL : $TOTAL = '';
            $TELEPON = "";  //KARENA BELUM ADA, MASI PAKAI DARI DB

            if ($id_user != '' && $user_token != '' && $NO_FAKTUR != '' && $KODE != ''
                    && $TOTAL != '') {
                $is_login = $this->Apikolektormodel->getUserByToken($id_user, $user_token);
                if ($is_login) {

                    $tanggal = date('Y-m-d');
                    $tanggal_id = date('ymd');
                    $pembayaran_ke = ($this->Apikolektormodel->totalPembayaranHariIni($tanggal_id) + 1);
                    $id_pembayaran = "";

                    if ($pembayaran_ke < 10) {
                        $id_pembayaran = $tanggal_id . '000' . $pembayaran_ke;
                    } else if ($pembayaran_ke >= 10 && $pembayaran_ke < 100) {
                        $id_pembayaran = $tanggal_id . '00' . $pembayaran_ke;
                    } else if ($pembayaran_ke >= 100 && $pembayaran_ke < 1000) {
                        $id_pembayaran = $tanggal_id . '0' . $pembayaran_ke;
                    } else {
                        $id_pembayaran = $tanggal_id . '' . $pembayaran_ke;
                    }

                    $insert_pembayaran = $this->Apikolektormodel->insert_pembayaran($id_pembayaran, $tanggal, $id_user, $KODE, $TELEPON, $NO_FAKTUR, $TOTAL);

                    if ($insert_pembayaran) {
                        $nomorHP = $this->Apikolektormodel->getNoHPToko($KODE);
                        $message = $this->Apikolektormodel->getSMSKoletor();
                        if (count($message) == 1 && $nomorHP != "") {
                            $textMessage = $message->MESSAGES . " Jumlah pembayaran anda adalah " .
                                    $this->convert_to_rupiah($TOTAL) . ",-.";
                            $this->sendNotificationSMSPelunasan($nomorHP, $textMessage);
                        } else {
                            //tidak mengirim pesan SMS, kolom telepon/sms kosong
                        }

                        $message = "Pembayaran berhasil disimpan. Total " . $this->convert_to_rupiah($TOTAL) . ",-.";
                        $data = null;
                        $status = 1;
                    } else {
                        $message = "Gagal menyimpan data pembayaran.";
                        $data = null;
                        $status = 0;
                    }
                } else {
                    $message = 'Akun tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }

    function convert_to_rupiah($angka) {
        return 'Rp. ' . strrev(implode('.', str_split(strrev(strval($angka)), 3)));
    }

    function scanTokoPembayaranFaktur() {
        //untuk mengecek apakah input dari aplikasi android
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->KODE) ? $KODE = $data->KODE : $KODE = '';

            if ($id_user != '' && $user_token != '' && $KODE != '') {
                //mengecek waktu scan, harus diantara jam 08.00 - 15.30
                $is_login = $this->Apikolektormodel->getUserByToken($id_user, $user_token);
                if ($is_login != false) {
                    //mengecek apakah ID Sales sesuai dengan toko atau tidak
                    $toko = $this->Apikolektormodel->getTokobyKode($KODE);

                    if (count($toko) == 1) {
                        if ($toko[0]->KDKOLEKTOR == $id_user) {
                            $poinToko = $this->Apikolektormodel->getPoin($KODE);
                            $piutangToko = $this->Apikolektormodel->getPiutang($KODE);
                            $invoiceToko = $this->Apikolektormodel->getInvoiceToko($KODE, $id_user);
                            $keterangan = $this->Apikolektormodel->getKoletorKeterangan();
                            if ($poinToko) {
                                $newdata = array(
                                    'poin' => $poinToko,
                                    'piutang' => $piutangToko,
                                    'invoice' => $invoiceToko,
                                    'keterangan' => $keterangan,
                                );
                                $message = "Scan berhasil.";
                                $data = $newdata;
                                $status = 1;
                            } else {
                                $message = "Data toko tidak ditemukan.";
                                $data = null;
                                $status = 0;
                            }
                        } else {
                            $message = 'Maaf, barcode yang anda scan bukan pelanggan anda.';
                            $data = null;
                            $status = 0;
                        }
                    } else {
                        $message = 'Data Toko tidak ditemukan.';
                        $data = null;
                        $status = 0;
                    }
                } else {
                    $message = 'Akun tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }

    function pembayaranFaktur() {
        //untuk mengecek apakah input dari aplikasi android
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->JENIS_PEMBAYARAN) ? $JENIS_PEMBAYARAN = $data->JENIS_PEMBAYARAN : $JENIS_PEMBAYARAN = '';
            isset($data->MODEL_PEMBAYARAN) ? $MODEL_PEMBAYARAN = $data->MODEL_PEMBAYARAN : $MODEL_PEMBAYARAN = '';
            isset($data->INVOICE)       ? $INVOICE      = $data->INVOICE        : $INVOICE = array();
            isset($data->INVOICE_AKSI)  ? $INVOICE_AKSI = $data->INVOICE_AKSI   : $INVOICE_AKSI = array();
            isset($data->INVOICE_BAYAR) ? $INVOICE_BAYAR= $data->INVOICE_BAYAR  : $INVOICE_BAYAR = array();
            isset($data->KODE) ? $KODE = $data->KODE : $KODE = '';
            isset($data->TOTAL) ? $TOTAL = $data->TOTAL : $TOTAL = '';
            isset($data->TELEPON) ? $TELEPON = $data->TELEPON : $TELEPON = '';
            isset($data->IS_BAYAR) ? $IS_BAYAR = $data->IS_BAYAR : $IS_BAYAR = '';
            isset($data->ID_KET) ? $ID_KET = $data->ID_KET : $ID_KET = '';

            $status_form = true;
            if (!($id_user != '' && $user_token != '' && $KODE != '' && $JENIS_PEMBAYARAN != '' && $MODEL_PEMBAYARAN != ''
                    && $TELEPON != '')){
                        $status_form = false;
                        $message = "Data Form User Tidak Lengkap";
            }
            if($JENIS_PEMBAYARAN == "1"){
                if(count($INVOICE) == 0  || count($INVOICE_AKSI) == 0 || count($INVOICE_BAYAR) == 0){
                    $status_form = false;
                    $message = "Data Jenis Pembayaran Tidak Lengkap";
                }else{
                    $TOTAL = 0;
                    for($i=0; $i < count($INVOICE_BAYAR); $i++){
                        $TOTAL = $TOTAL + $INVOICE_BAYAR[$i];
                    }
                }
            }elseif($JENIS_PEMBAYARAN == 2 && $TOTAL == ""){
                $status_form = false;
                $message = "Data Jenis Pembayaran Total Tidak Lengkap";
            }
            if($IS_BAYAR == "0" && $ID_KET == ""){
                $status_form = false;
                $message = "Data Pembayaran Tidak Lengkap";
            }

            if ($status_form) {
                $is_login = $this->Apikolektormodel->getUserByToken($id_user, $user_token);
                if ($is_login) {

                    $tanggal = date('Y-m-d');
                    $tanggal_id = date('ymd');
                    $pembayaran_ke = ($this->Apikolektormodel->totalPembayaranHariIni($tanggal_id) + 1);
                    $id_pembayaran = "";

                    if ($pembayaran_ke < 10) {
                        $id_pembayaran = $tanggal_id . '000' . $pembayaran_ke;
                    } else if ($pembayaran_ke >= 10 && $pembayaran_ke < 100) {
                        $id_pembayaran = $tanggal_id . '00' . $pembayaran_ke;
                    } else if ($pembayaran_ke >= 100 && $pembayaran_ke < 1000) {
                        $id_pembayaran = $tanggal_id . '0' . $pembayaran_ke;
                    } else {
                        $id_pembayaran = $tanggal_id . '' . $pembayaran_ke;
                    }

                    $tgl_pembayaran = date('Y-m-d H:i:s');

                    if (substr($TELEPON, 0, 2) == "08") {
                        $akhir = substr($TELEPON, 2, strlen($TELEPON) - 2);
                        $Telp_baru = "+628" . $akhir;
                        $TELEPON = $Telp_baru;
                    }

                    $insert_pembayaran = $this->Apikolektormodel->insert_pembayaran($id_pembayaran, $tgl_pembayaran, 
                    $id_user, $KODE, $TELEPON, $JENIS_PEMBAYARAN, $MODEL_PEMBAYARAN, $TOTAL, $IS_BAYAR, $ID_KET);

                    if ($insert_pembayaran) {
                        //insert detail order berhasil
                        //menyimpan data invoice jika JENIS_PEMBAYARAN = 1 (INVOICE) dan invoice tidak boleh 0
                        //mmengubah t_penjualan where invoice 
                        if ($JENIS_PEMBAYARAN == 1 && count($INVOICE) > 0) {
                            $datetime = date('Y-m-d H:i:s');
                            for ($i = 0; $i < count($INVOICE); $i++) {
                                $cicilan_ke     = ($this->Apikolektormodel->totalCicilanKe($INVOICE[$i]) + 1);
                                $this->Apikolektormodel->insert_pembayaran_kolektor_invoice($id_pembayaran, $INVOICE[$i], $cicilan_ke, $INVOICE_BAYAR[$i]);
                                $this->Apikolektormodel->update_t_penjualan($id_user, $tgl_pembayaran, $id_pembayaran, $INVOICE[$i], $INVOICE_AKSI[$i]);
                            }
                        }

                        //mengirim sms notifikasi ke nomor pelanggan
                        $nomorHP = $TELEPON;
                        $message = $this->Apikolektormodel->getSMSKoletor();
                        if (!is_null($message) && $nomorHP != "") {
                            if ($IS_BAYAR == "1") {
                                $textMessage = $message->MESSAGES . " Jumlah pembayaran anda adalah " .
                                        $this->convert_to_rupiah($TOTAL) . ",-.";
                            } else {
                                $textMessage = "Anda belum melakukan pembayaran. Jumlah yang harus dibayar adalah " .
                                        $this->convert_to_rupiah($TOTAL) . ",-.";
                            }
                            $this->sendNotificationSMSPelunasan($nomorHP, $textMessage);
                        } else {
                            //tidak mengirim pesan SMS, kolom telepon/sms kosong
                        }

                        if ($IS_BAYAR == "1") {
                            $message = "Pembayaran berhasil disimpan. Total " . $this->convert_to_rupiah($TOTAL) . ",-.";
                        } else {
                            $message = "Data berhasil disimpan. Pelanggan belum melakukan pembayaran";
                        }
                        $data = null;
                        $status = 1;
                    } else {
                        $message = "Gagal menyimpan data pembayaran.";
                        $data = null;
                        $status = 0;
                    }
                } else {
                    $message = 'Akun tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }

    function insert_t_penjualan() {
        //untuk mengecek apakah input dari aplikasi android
        if ($this->checkInsPenjualan($this->input->get_request_header('appJualName'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));


            isset($data->kd_cabang) ? $kd_cabang = $data->kd_cabang : $kd_cabang = '';
            isset($data->tanggal) ? $tanggal = $data->tanggal : $tanggal = '';
            isset($data->bukti) ? $bukti = $data->bukti : $bukti = '';
            isset($data->kd_lang) ? $kd_lang = $data->kd_lang : $kd_lang = '';
            isset($data->kd_sub) ? $kd_sub = $data->kd_sub : $kd_sub = '';
            isset($data->jenis_bayar) ? $jenis_bayar = $data->jenis_bayar : $jenis_bayar = '';
            isset($data->jenis_kirim) ? $jenis_kirim = $data->jenis_kirim : $jenis_kirim = '';
            isset($data->tgl_kirim) ? $tgl_kirim = $data->tgl_kirim : $tgl_kirim = '';
            isset($data->ref_order) ? $ref_order = $data->ref_order : $ref_order = '';
            isset($data->kd_barang) ? $kd_barang = $data->kd_barang : $kd_barang = '';
            isset($data->jml_kwt) ? $jml_kwt = $data->jml_kwt : $jml_kwt = '';
            isset($data->hrg_sat) ? $hrg_sat = $data->hrg_sat : $hrg_sat = '';
            isset($data->jml_hrg) ? $jml_hrg = $data->jml_hrg : $jml_hrg = '';
            isset($data->disc_sat) ? $disc_sat = $data->disc_sat : $disc_sat = '';
            isset($data->disc_tot) ? $disc_tot = $data->disc_tot : $disc_tot = '';
            isset($data->kg) ? $kg = $data->kg : $kg = '';
            isset($data->tonase) ? $tonase = $data->tonase : $tonase = '';
            isset($data->jenis_barang) ? $jenis_barang = $data->jenis_barang : $jenis_barang = '';
            isset($data->ref_spj) ? $ref_spj = $data->ref_spj : $ref_spj = '';
            isset($data->tempo) ? $tempo = $data->tempo : $tempo = '';
            isset($data->tgl_jtempo) ? $tgl_jtempo = $data->tgl_jtempo : $tgl_jtempo = '';
            isset($data->hari) ? $hari = $data->hari : $hari = '';
            isset($data->tgl_jtempo_spj) ? $tgl_jtempo_spj = $data->tgl_jtempo_spj : $tgl_jtempo_spj = '';
            isset($data->nopol) ? $nopol = $data->nopol : $nopol = '';
            isset($data->driver) ? $driver = $data->driver : $driver = '';
            isset($data->tgl_insert) ? $tgl_insert = $data->tgl_insert : $tgl_insert = '';
            isset($data->user_insert) ? $user_insert = $data->user_insert : $user_insert = '';
            isset($data->flag_hapus) ? $flag_hapus = $data->flag_hapus : $flag_hapus = '';

            if ($bukti != '' && $kd_lang != '' && $jml_hrg != '') {

                $insert_t_penjualan = $this->Apikolektormodel->insert_t_penjualan($kd_cabang, $tanggal, $bukti, $kd_lang, $kd_sub, $jenis_bayar, $jenis_kirim, $tgl_kirim, $ref_order, $kd_barang, $jml_kwt, $hrg_sat, $jml_hrg, $disc_sat, $disc_tot, $kg, $tonase, $jenis_barang, $ref_spj, $tempo, $tgl_jtempo, $hari, $tgl_jtempo_spj, $nopol, $driver, $tgl_insert, $user_insert, $flag_hapus);

                if ($insert_t_penjualan) {
                    $data = null;
                    $status = 1;
                } else {
                    $message = "Gagal menyimpan data pembayaran.";
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }
    public function getHistoryPembayaran() {
        if ($this->checkApp($this->input->get_request_header('appToken'))) {
            $status = -1;
            $message = '';
            $data = '';

            $data = (object) json_decode(file_get_contents('php://input'));

            isset($data->id_user) ? $id_user = $data->id_user : $id_user = '';
            isset($data->user_token) ? $user_token = $data->user_token : $user_token = '';

            isset($data->TGL_AWAL) ? $TGL_AWAL = $data->TGL_AWAL : $TGL_AWAL = '';
            isset($data->TGL_AKHIR) ? $TGL_AKHIR = $data->TGL_AKHIR : $TGL_AKHIR = '';
            isset($data->START) ? $START = $data->START : $START = '';
            isset($data->LIMIT) ? $LIMIT = $data->LIMIT : $LIMIT = '';


            if ($id_user != '' && $user_token != '') {
                //mengecek login user
                $is_login = $this->Apikolektormodel->getUserByToken($id_user, $user_token);
                if ($is_login) {
                    $data_pembayaran = $this->Apikolektormodel->getHistoryPembayaran($id_user, $TGL_AWAL, $TGL_AKHIR, $START, $LIMIT);
                    
                    if ($data_pembayaran) {
                        $pembayaran = array(
                            "total" => count($data_pembayaran),
                            "pembayaran" => $data_pembayaran
                        );
                        $message = 'Data Pembayaran.';
                        $data = $pembayaran;
                        $status = 1;
                    } else {
                        $message = 'Data Pembayaran tidak ditemukan.';
                        $data = null;
                        $status = 0;
                    }
                } else {
                    $message = 'Akun tidak ditemukan.';
                    $data = null;
                    $status = 0;
                }
            } else {
                $message = 'Field is empty.';
                $data = null;
                $status = 0;
            }
            $response = array(
                'status' => $status,
                'message' => $message,
                'data' => $data
            );
            $this->makeOutput($response);
        } else {
            $this->jsonNoRespon();
        }
    }

}