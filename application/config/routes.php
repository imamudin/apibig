<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//myroute
$route['login']['post']							= 'permission/login';
$route['logout']['post']						= 'permission/logout';

$route['scantoko']['post']						= 'api/scantoko';
$route['tutupkunjungan']['post']				= 'api/tutupKunjungan';
$route['forminputstok']['get']					= 'api/formInputStok';
$route['inputstok']['post']						= 'api/inputStok';
$route['cekfaktur']['post']						= 'api/cekFaktur';
$route['simpanorder']['post']					= 'api/simpanOrder';

//untuk order non semen
$route['cekfakturnonsemen']['post']				= 'api/cekFakturNonSemen';
$route['simpanordernonsemen']['post']			= 'api/simpanOrderNonSemen';

$route['kunjungans']['post']					= 'api/getKunjungan';
$route['orders']['post']						= 'api/getOrder';
$route['detailorder']['post']					= 'api/getDetailOrder';

//untuk order non semen
$route['orders_nonsemen']['post']				= 'api/getOrderNonSemen';
$route['detailorder_nonsemen']['post']			= 'api/getDetailOrderNonSemen';

$route['tokos']['post']							= 'api/getToko';
$route['piutangs']['post']						= 'api/getPiutang';
$route['detailpiutang']['post']					= 'api/getDetailPiutang';
$route['target']['post']						= 'api/getTarget';

$route['poin']['post']							= 'api/getPoin';
$route['detailpoin']['post']					= 'api/getDetailPoin';
$route['detailtukar']['post']					= 'api/getDetailTukar';
$route['pembayaran']['post']					= 'api/getPembayaran';
$route['detail_pelunasan']['post']				= 'api/getDetailPelunasan';
$route['bayar_pelunasan']['post']				= 'api/bayarPelunasan';


$route['updatekegiatan']['post']				= 'api/updateKegiatan';
$route['scantokotanpabarcode']['post']			= 'api/scantokotanpabarcode';

$route['teskoneksi']['get']					    = 'api/teskoneksi';


//pai untuk admin
$route['loginadmin']['post']					= 'apiadmin/login';
$route['logoutadmin']['post']					= 'apiadmin/logout';
$route['scantokoadmin']['post']					= 'apiadmin/scantoko';
$route['updatekoordinat']['post']				= 'apiadmin/updatekoordinat';
$route['tokos_admin']['post']					= 'apiadmin/getToko';


//ini untuk apitoko
$route['logintoko']['post']						= 'apitoko/login';
$route['logouttoko']['post']					= 'apitoko/logout';
$route['pembayaranstoko']['post']				= 'apitoko/getPembayaran';
$route['kunjunganstoko']['post']				= 'apitoko/getKunjungan';
$route['orderstoko']['post']					= 'apitoko/getOrder';
$route['detailordertoko']['post']				= 'apitoko/getDetailOrder';
$route['tokostoko']['post']						= 'apitoko/getToko';
$route['piutangstoko']['post']					= 'apitoko/getPiutang';
$route['detailpiutangtoko']['post']				= 'apitoko/getDetailPiutang';

$route['datapelanggan']['post']					= 'apitoko/getDataPelanggan';

$route['piutang_toko']['post']					= 'apitoko/getPiutang';
$route['detail_piutang_toko']['post']			= 'apitoko/getDetailPiutang';

$route['poin_toko']['post']					    = 'apitoko/getPoin';
$route['detailpoin_toko']['post']				= 'apitoko/getDetailPoin';
$route['detailtukar_toko']['post']				= 'apitoko/getDetailTukar';


$route['detail_pelunasan_toko']['post']			= 'apitoko/getDetailPelunasan';
$route['bayar_pelunasan_toko']['post']			= 'apitoko/bayarPelunasan';


//untuk aplikasi kolektor
$route['login_kolektor']['post']				= 'apikolektor/login';
$route['logout_kolektor']['post']				= 'apikolektor/logout';
$route['toko_kolektor']['post']					= 'apikolektor/getToko';
$route['piutang_kolektor']['post']				= 'apikolektor/getPiutang';
$route['detail_piutang_koletor']['post']		= 'apikolektor/getDetailPiutang';
$route['daftar_pelunasan_kolektor']['post']		= 'apikolektor/getDaftarPelunasan'; //sudah tidak digunakan
$route['pembayaran_tagihan']['post']		    = 'apikolektor/pembayaranTagihan';
$route['insert_t_penjualan']['post']		    = 'apikolektor/insert_t_penjualan';
$route['history_pembayaran_kolektor']['post']	= 'apikolektor/getHistoryPembayaran';

//pembayaran faktur
$route['pembayaranfaktur']['post']		        = 'apikolektor/pembayaranFaktur';
$route['scantokopembayaran']['post']		    = 'apikolektor/scanTokoPembayaranFaktur';

//untuk kadepo
$route['login_kadepo']['post']					= 'apikadepo/login';
$route['logout_kadepo']['post']					= 'apikadepo/logout';

$route['transaksi_kadepo']['post']				= 'apikadepo/transaksi';
$route['cekfaktur_kadepo']['post']				= 'apikadepo/cekFaktur';								
$route['simpanorder_kadepo']['post']			= 'apikadepo/simpanOrder';								

//untuk order non semen
$route['cekfaktur_kadepo_nonsemen']['post']		= 'apikadepo/cekFakturNonSemen';
$route['simpanorder_kadepononsemen']['post']	= 'apikadepo/simpanOrderNonSemen';

$route['orders_kadepo']['post']					= 'apikadepo/getOrder';
$route['detailorder_kadepo']['post']			= 'apikadepo/getDetailOrder';								
//untuk order non semen
$route['orders_kadepo_non_semen']['post']		= 'apikadepo/getOrderNonSemen';
$route['detailorder_kadepo_non_semen']['post']	= 'apikadepo/getDetailOrderNonSemen';


$route['tokos_kadepo']['post']					= 'apikadepo/getToko';
$route['piutangs_kadepo']['post']				= 'apikadepo/getPiutang';								
$route['detailpiutang_kadepo']['post']			= 'apikadepo/getDetailPiutang';								
$route['target_kadepo']['post']					= 'apikadepo/getTarget';								

$route['poin_kadepo']['post']					= 'apikadepo/getPoin';								
$route['detailpoin_kadepo']['post']				= 'apikadepo/getDetailPoin';						
$route['detailtukar_kadepo']['post']			= 'apikadepo/getDetailTukar';						
$route['pembayaran_kadepo']['post']				= 'apikadepo/getPembayaran';						
$route['detail_pelunasan_kadepo']['post']		= 'apikadepo/getDetailPelunasan';					
$route['bayar_pelunasan_kadepo']['post']		= 'apikadepo/bayarPelunasan';						

